import QtQuick 2.12

Rectangle {
    id: bulb
    width: 90
    height: 90
    color: "gray"
    border.color: "black"
    border.width: 1
    radius: 100
}