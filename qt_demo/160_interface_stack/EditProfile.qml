import QtQuick 2.12
import QtQuick.Controls 2.4

Page {
    title: qsTr("EditProfile")

    Label {
        
        anchors.centerIn: parent
        text: qsTr("Editing the profile")
    }
}