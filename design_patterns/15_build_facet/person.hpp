#ifndef _FREDIRC_PERSON_ALL_H_
#define  _FREDIRC_PERSON_ALL_H_

#include "person.h"
#include "person_builder.h"
#include "person_address_builder.h"
#include "person_job_builder.h"

#endif