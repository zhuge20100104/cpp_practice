#ifndef _FREDRIC_ADDER_H_
#define _FREDRIC_ADDER_H_

namespace my_math {
    int add(int a, int b);
    float add(float a, float b);
}

#endif