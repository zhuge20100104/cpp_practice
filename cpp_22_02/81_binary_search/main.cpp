#include <deque>
#include <iostream>
#include <algorithm>
#include <set>

template <class T>
struct Out {
    std::ostream& out;
    Out(std::ostream& o): out(o) {}

    void operator()(T const& val) {
        out << val << " ";
    }
};

int main(int argc, char* argv[]) {
    int t[] = {8, 10, 5, 1, 4, 6, 2, 7, 9, 3};

    std::deque<int> d1(t, t+10);
    std::set<int> s1(t, t+10);

    // Expect output: 1 0 
    // 二分查找必须要先排序
    std::cout << std::binary_search(s1.begin(), s1.end(), 4) << " "
        << std::binary_search(d1.begin(), d1.end(), 4) << std::endl;

    return EXIT_SUCCESS;
}