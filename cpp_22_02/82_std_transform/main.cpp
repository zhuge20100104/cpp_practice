#include <vector>
#include <iostream>
#include <algorithm>
#include <functional> // functional用来使用std::bind


template <class T>
struct Out {
    std::ostream& out;
    Out(std::ostream& o): out(o) {}

    void operator()(T const& val) {
        out << val << " ";
    }
};


struct Add {
    using second_argument_type = int;
    using first_argument_type = int;
    using result_type = int;
    // int operator()(int const& a, int const& b) const {
    //     return a + b;
    // }

    int operator()(int& a, int const& b) const {
        return a + b;
    }
};

int main(int argc, char* argv[]) {
    int t[] = {1, 2, 3, 4, 5, 6, 7, 8, 9, 10};

    std::vector<int> v1(t, t+10);

    std::vector<int> v2(10);

    // std::transform(v1.begin(), v1.end(), v2.begin(), std::bind(Add(), std::placeholders::_1, 1));

    std::plus<int>();
    std::transform(v1.begin(), v1.end(), v2.begin(), std::bind2nd(Add(),  1));
    // Expect output: 11 10 9 8 7 6 5 4 3 2

    // 正儿八经写好是上面的输出，瞎写就是compilation error.
    std::for_each(v2.rbegin(), v2.rend(), Out<int>(std::cout));

    std::cout << std::endl;

    return EXIT_SUCCESS;
}