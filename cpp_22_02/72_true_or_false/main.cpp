#include <cassert>
#include <iostream>
#include <vector>
#include <fstream>
#include <algorithm>
#include <numeric>
#include <cmath>
#include <string>
#include <map>
#include <set>
#include <deque>
#include <list>
#include <stack>
#include <forward_list>
#include <iomanip>


int main(int argc, char* argv[]) {
  
  bool a, b;
  std::cin >> std::boolalpha >> a >> b;
  // Input: true false
  // Expect output 10
  std::cout << a << b << std::endl;

  return EXIT_SUCCESS;
} 

