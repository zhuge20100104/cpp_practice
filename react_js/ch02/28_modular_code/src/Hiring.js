import React from "react"
// 函数返回除了 {return XX} 外，也可以直接写tag
const Hiring = () => 
  <div>
    <p>The Library is hiring. Go to www.library.com/jobs for more.</p>
  </div>

export default Hiring