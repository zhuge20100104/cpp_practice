import React from "react"
// 只要用到JSX就要引入React

const NotHiring = () => 
  <div>
    <p>The Library is not hiring. Check back later for more.</p>
  </div>

export default NotHiring