#include <cstdio>

int main(int argc, char* argv[]) {
    int twos[5] = {2, 4, 6, 8, 10};

    printf("%d\n", twos[0]);
    printf("%d\n", twos[1]);
    printf("%d\n", twos[2]);
    printf("%d\n", twos[3]);
    printf("%d\n", twos[4]);
    
    return 0;
}