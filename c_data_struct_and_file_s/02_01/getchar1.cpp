#include <cstdio>

int main(int argc, char* argv[]) {
    int ch;
    printf("Type a character: ");
    ch = getchar();
    printf("Character '%c' received.\n", ch);
    return 0;
}

