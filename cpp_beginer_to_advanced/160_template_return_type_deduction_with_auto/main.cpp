﻿#include <iostream>

template <class T, class P>
auto maximum(T a, P b) {
    return (a > b) ? a: b;
}

// Next chapter: 24.8 decltype and trailing return types
int main(int argc, char* argv[]) {
    std::cout << sizeof(maximum(2, 3.2)) << '\n';

    std::cout << sizeof(maximum<int, int>(2, 3.2)) << '\n';
    return EXIT_SUCCESS;
}