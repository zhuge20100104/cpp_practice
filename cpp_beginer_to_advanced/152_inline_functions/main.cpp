﻿#include <iostream>

inline int mymax(int a, int b) {
    return (a > b)? a: b;
}

int main(int argc, char* argv[]) {
    std::cout << mymax(11, 9) << '\n';
    return EXIT_SUCCESS;
}