﻿#include <iostream>

template <class T, class P>
decltype(auto) maximum(T a, P b) {
    return (a > b) ? a : b;
}


// 24.10 default_arguments_with_function_templates
int main(int argc, char* argv[]) {
    std::cout << maximum(2, 8) << '\n';
    return EXIT_SUCCESS;
}
