// 导入 iostream 库
#include <iostream>


// 注意，块注释不能嵌套

int main(int argc, char* argv[]) {
    /*
        这是可以跨越多行的
        块注释
    */

   // 打印 Hello World 到控制台
    std::cout << "Hello, World!" << std::endl;
    return EXIT_SUCCESS;
}