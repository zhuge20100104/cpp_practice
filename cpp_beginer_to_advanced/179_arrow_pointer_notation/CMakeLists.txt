﻿cmake_minimum_required(VERSION 3.5)

project(179_arrow_pointer_notation LANGUAGES CXX)

set(CMAKE_CXX_STANDARD 20)
set(CMAKE_CXX_STANDARD_REQUIRED ON)

add_executable(179_arrow_pointer_notation
    main.cpp)

target_include_directories(179_arrow_pointer_notation PRIVATE
    ${CMAKE_CURRENT_SOURCE_DIR}/beans    
    )

include(GNUInstallDirs)
install(TARGETS 179_arrow_pointer_notation
    LIBRARY DESTINATION ${CMAKE_INSTALL_LIBDIR}
    RUNTIME DESTINATION ${CMAKE_INSTALL_BINDIR}
)
