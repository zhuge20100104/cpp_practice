﻿#include <iostream>
#include <concepts>
#include <type_traits>
#include <memory>


double const c_pi = 3.14;

class cylinder_t {
public:

    cylinder_t() = default; 

    cylinder_t(double radius, double height):
        m_base_radius(radius),
        m_height(height)
     {}

    auto get_radius() {
        return m_base_radius;
    }

    auto get_height() {
        return m_height;
    }

    void set_radius(double radius) {
        m_base_radius = radius;
    }

    void set_height(double height) {
        m_height = height;
    }

    double volume() {
        return c_pi * m_base_radius * m_base_radius * m_height;
    }

private:
    double m_base_radius {};
    double m_height {};
};


int main(int argc, char* argv[]) {
  
    cylinder_t c1 {5, 10};
    std::cout << c1.volume() << '\n';

    std::unique_ptr<cylinder_t> p_c2 = std::make_unique<cylinder_t>(2, 4);

    std::cout << (*p_c2).volume() << '\n';

    std::cout << p_c2->volume() << '\n';

    return EXIT_SUCCESS;
}
