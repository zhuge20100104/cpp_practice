﻿#include <iostream>

template <int threshold, class T>
bool is_valid(T collection[], std::size_t size) {
    T sum {};

    for(std::size_t i=0; i<size; ++i) {
        sum += collection[i];
    }

    return sum > threshold;
}

int main(int argc, char* argv[]) {
    double temperatures[] {10.0, 20.0, 30.0, 40.0, 50.0};

    auto result {is_valid<200, double>(temperatures, std::size(temperatures))};
    std::cout << result << '\n';
    
    return EXIT_SUCCESS;
}
