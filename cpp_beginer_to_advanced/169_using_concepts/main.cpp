﻿#include <iostream>
#include <concepts>


template <class T>
T sum(T a, T b) requires std::integral<T> {
    return a + b;
}

// Next chapter: 25.2 Building your own concepts
int main(int argc, char* argv[]) {
    sum(2, 2);
    return EXIT_SUCCESS;
}
