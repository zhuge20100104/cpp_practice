﻿#include <iostream>
#include <cstring>

template <class T>
T maximum(T a, T b) {
    std::cout << "\ndefault template called\n";
    return (a > b) ? a: b;
}

// specialized template for char const* type
template <>
char const* maximum(char const* a, char const* b) {
    std::cout << "\nspecialization called\n";
    return (std::strcmp(a, b) > 0) ? a : b;
}


int main(int argc, char* argv[]) {
    double d1 {22.38};
    double d2 {98.27};

    std::cout << maximum(d1, d2) << '\n';

    char const* h {"hello"};
    char const* w {"wello"};

    std::cout << maximum(h, w) << '\n';

    return EXIT_SUCCESS;
}