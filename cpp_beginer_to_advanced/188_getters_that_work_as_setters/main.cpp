﻿#include <iostream>
#include <concepts>
#include <type_traits>
#include <memory>
#include <string_view>

class dog_t {
public:
    dog_t() = default;
    dog_t(std::string_view name, std::string_view breed, unsigned int age);
    dog_t(dog_t const& rhs);

    std::string& name() {
        return this->m_name;
    }

    std::string& breed() {
        return this->m_breed;
    }

    unsigned int& age() {
        return *(this->m_p_age);
    }

    std::string name() const {
        return m_name;
    }

    std::string breed() const {
        return m_breed;
    }

    unsigned int get_age() const {
        return *m_p_age;
    }

    void print_dog() const;
    ~dog_t();

private:
    std::string m_name {""};
    std::string m_breed {""};
    unsigned int* m_p_age {nullptr}; 
};


dog_t::dog_t(std::string_view name, std::string_view breed, unsigned int age):
    m_name(name), m_breed(breed), m_p_age(new unsigned int(age)) {}

dog_t::dog_t(dog_t const& rhs):m_name(rhs.m_name), m_breed(rhs.m_breed), m_p_age(new unsigned int(*rhs.m_p_age)) {

}

void dog_t::print_dog() const {
    std::cout << "Name: " << m_name << '\n';
    std::cout << "Breed: " << m_breed << '\n';
    std::cout << "Age: " << *m_p_age << '\n';
}

dog_t::~dog_t() {
    delete m_p_age;
}


// Next one: 27.5 dangling pointers and references in objects
int main(int argc, char* argv[]) {
  
    dog_t dog {"Tom", "Lebra", 3};
    dog.print_dog();

    dog.name() = "Gom";
    dog.breed() = "Debra";
    dog.age() = 2;

    dog.print_dog();

    dog_t const d2 {"test", "tet", 22};
    std::cout << d2.name() << '\n';
    return EXIT_SUCCESS;
}
