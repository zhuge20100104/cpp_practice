﻿#include <iostream>

std::size_t sumup_to_zero(std::size_t value) {
    if(value != 0) {
        return value + sumup_to_zero(value - 1);
    }
    return 0;
}


// Next chapter: 24.1 trying out function templates.cpp
int main(int argc, char* argv[]) {
    std::cout << sumup_to_zero(5) << '\n';

    return EXIT_SUCCESS;
}