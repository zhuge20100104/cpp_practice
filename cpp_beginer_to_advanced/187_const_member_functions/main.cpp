﻿#include <iostream>
#include <concepts>
#include <type_traits>
#include <memory>
#include <string_view>

class dog_t {
public:
    dog_t() = default;
    dog_t(std::string_view name, std::string_view breed, unsigned int age);
    dog_t(dog_t const& rhs);

    dog_t* set_name(std::string_view name) {
        this->m_name = name;
        return this;
    }

    dog_t* set_breed(std::string_view breed) {
        this->m_breed = breed;
        return this;
    }

    dog_t* set_age(unsigned int age) {
        *(this->m_p_age) = age;
        return this;
    }

    std::string_view get_name() const {
        return m_name;
    }

    std::string_view get_breed() const {
        return m_breed;
    }

    unsigned int get_age() const {
        return *m_p_age;
    }

    void print_dog() const;
    ~dog_t();

private:
    std::string m_name {""};
    std::string m_breed {""};
    unsigned int* m_p_age {nullptr}; 
};


dog_t::dog_t(std::string_view name, std::string_view breed, unsigned int age):
    m_name(name), m_breed(breed), m_p_age(new unsigned int(age)) {}

dog_t::dog_t(dog_t const& rhs):m_name(rhs.m_name), m_breed(rhs.m_breed), m_p_age(new unsigned int(*rhs.m_p_age)) {

}

void dog_t::print_dog() const {
    std::cout << "Name: " << m_name << '\n';
    std::cout << "Breed: " << m_breed << '\n';
    std::cout << "Age: " << *m_p_age << '\n';
}

dog_t::~dog_t() {
    delete m_p_age;
}


// Next one: 27.3 const member functions
int main(int argc, char* argv[]) {
  
    dog_t const d1 {"Doomed", "Damned", 2000};
    std::cout << "Age: " << d1.get_age() << '\n';
    d1.print_dog();

    return EXIT_SUCCESS;
}
