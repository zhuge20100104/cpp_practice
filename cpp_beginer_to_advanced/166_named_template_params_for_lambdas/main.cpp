﻿#include <iostream>


int main(int argc, char* argv[]) {
    auto add {
        []<class T, class P>(T a, P b) {
            return a + b;
        }
    };

    int a {22};
    double b {3.98};

    auto result(add(a, b));
    std::cout << result << '\n';

    auto result_int(add(2, 2));
    std::cout << result_int << '\n';

    return EXIT_SUCCESS;
}
