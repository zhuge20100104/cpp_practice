﻿#include <iostream>

template <class return_type = double, class T, class P>
return_type maximum(T a, P b) {
    return (a > b) ? a : b;
}

int main(int argc, char* argv[]) {
    int a {291};
    int b {782};

    // 8
    std::cout << sizeof(maximum(a, b)) << '\n';

    // 4
    std::cout << sizeof(maximum<float>(a, b)) << '\n';

    // 4
    std::cout << sizeof(maximum<int, double, double>(a, b)) << '\n';
    
    return EXIT_SUCCESS;
}
