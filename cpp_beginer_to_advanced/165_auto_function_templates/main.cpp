﻿#include <iostream>

auto sum(auto a, auto b) {
    return a + b;
}

int main(int argc, char* argv[]) {
    std::cout << sum(20, 20.2) << '\n';
    return EXIT_SUCCESS;
}
