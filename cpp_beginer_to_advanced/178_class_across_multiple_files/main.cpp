﻿#include <iostream>
#include <concepts>
#include <type_traits>
#include "cylinder.h"


// Next: 26.6 arrow pointer call notation
int main(int argc, char* argv[]) {
    cylinder_t c1 {};
    c1.set_radius(2.0);
    c1.set_height(2.0);

    std::cout << c1.get_radius() << '\n';
    std::cout << c1.get_height() << '\n';
    
    std::cout << c1.volume() << '\n';

    return EXIT_SUCCESS;
}
