#include "cylinder.h"


cylinder_t::cylinder_t(double base_radius, double height):
    m_base_radius(base_radius),
    m_height(height) {

}

double cylinder_t::get_radius() {
    return m_base_radius;
}

double cylinder_t::get_height() {
    return m_height;
}


void cylinder_t::set_radius(double base_radius) {
    m_base_radius = base_radius;
}
    
void cylinder_t::set_height(double height) {
    m_height = height;
}

double cylinder_t::volume() {
    return c_pi * m_base_radius * m_base_radius * m_height;
}
