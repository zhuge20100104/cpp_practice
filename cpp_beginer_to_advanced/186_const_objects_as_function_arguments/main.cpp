﻿#include <iostream>
#include <concepts>
#include <type_traits>
#include <memory>
#include <string_view>

class dog_t {
public:
    dog_t() = default;
    dog_t(std::string_view name, std::string_view breed, unsigned int age);
    dog_t(dog_t const& rhs);

    dog_t* set_name(std::string_view name) {
        this->m_name = name;
        return this;
    }

    dog_t* set_breed(std::string_view breed) {
        this->m_breed = breed;
        return this;
    }

    dog_t* set_age(unsigned int age) {
        *(this->m_p_age) = age;
        return this;
    }

    std::string_view get_name() {
        return m_name;
    }

    std::string_view get_breed() {
        return m_breed;
    }

    unsigned int get_age() {
        return *m_p_age;
    }

    void print_dog() const;
    ~dog_t();

private:
    std::string m_name {""};
    std::string m_breed {""};
    unsigned int* m_p_age {nullptr}; 
};


dog_t::dog_t(std::string_view name, std::string_view breed, unsigned int age):
    m_name(name), m_breed(breed), m_p_age(new unsigned int(age)) {}

dog_t::dog_t(dog_t const& rhs):m_name(rhs.m_name), m_breed(rhs.m_breed), m_p_age(new unsigned int(*rhs.m_p_age)) {

}

void dog_t::print_dog() const {
    std::cout << "Name: " << m_name << '\n';
    std::cout << "Breed: " << m_breed << '\n';
    std::cout << "Age: " << *m_p_age << '\n';
}

dog_t::~dog_t() {
    delete m_p_age;
}


void take_by_value(dog_t d) {
    d.set_age(2);
    d.print_dog();
}

void take_by_ref(dog_t& d) {
    d.set_name("hell");
    d.print_dog();
}

// Const对象没法调用非const方法 set_name
// void take_by_const_ref(dog_t const& d) {
//     d.set_name("hell");
//     d.print_dog();
// }


void take_by_ptr(dog_t* d) {
    d->set_name("hei");
    d->print_dog();
}

// 指针常量，set_name不可用
// void take_by_ptr_const(dog_t const* d) {
//     d->set_name("hei");
//     d->print_dog();
// }


// Next one: 27.3 const member functions
int main(int argc, char* argv[]) {
  
    dog_t const d1 {"Lenn", "Pitbull", 5};
    take_by_value(d1);

    // 必须是dog_t const&才能传递 
    // take_by_ref(d1);

    // take_by_ptr(&d1);

    return EXIT_SUCCESS;
}
