﻿#include <iostream>
#include <cstring>

template <class T>
T maximum(T a, T b) {
    std::cout << "Non-pointer template overload called: ";
    return (a > b) ? a : b;
}

// template function overload - will be instantiated if pointer arguments are passed
template <class T>
T* maximum(T* a, T* b) {
    std::cout << "Pointer template overload called: ";
    return (*a > *b) ? a : b;
}

// normal overloaded function - will be used if arguments are c-strings
char const* maximum(char const* a, char const* b) {
    std::cout << "Normal overload called: ";
    return (std::strcmp(a, b) > 0) ? a: b;
}

// Next chapter: 24.6 - function templates with multiple parameters
int main(int argc, char* argv[]) {
    int i1 {39};
    int i2 {92};

    auto p_i1 {&i1};
    auto p_i2 {&i2};

    char const* h {"hello"};
    char const* w {"world"};

    std::cout << maximum(h, w) << '\n';
    std::cout << maximum(i1, i2) << '\n';
    std::cout << *maximum(p_i1, p_i2) << '\n';
    return EXIT_SUCCESS;
}