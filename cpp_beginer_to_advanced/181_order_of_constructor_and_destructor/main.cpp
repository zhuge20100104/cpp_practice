﻿#include <iostream>
#include <concepts>
#include <type_traits>
#include <memory>
#include <string_view>


class dog_t {
public:

    dog_t() = default;
    dog_t(std::string_view name, std::string_view breed, int age);

    ~dog_t();

private:
    std::string m_name {};
    std::string m_breed {};
    int* m_p_age {nullptr};
};


dog_t::dog_t(std::string_view name, std::string_view breed, int age):
    m_name(name), m_breed(breed), m_p_age(new int(age)) {
}

dog_t::~dog_t() {
    delete m_p_age;
    m_p_age = nullptr;

    std::cout << m_name << " has been killed\n";
}


int main(int argc, char* argv[]) {
  
    dog_t d1 {"Tom", "Desi", 2};
    dog_t d2 {"Tom2", "Desi2", 4};
    dog_t d3 {"Tom3", "Desi3", 8};
    dog_t d4 {"Tom4", "Desi4", 16};
    
    return EXIT_SUCCESS;
}
