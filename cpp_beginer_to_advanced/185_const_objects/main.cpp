﻿#include <iostream>
#include <concepts>
#include <type_traits>
#include <memory>
#include <string_view>

class dog_t {
public:
    dog_t() = default;
    dog_t(std::string_view name, std::string_view breed, unsigned int age);

    dog_t* set_name(std::string_view name) {
        this->m_name = name;
        return this;
    }

    dog_t* set_breed(std::string_view breed) {
        this->m_breed = breed;
        return this;
    }

    dog_t* set_age(unsigned int age) {
        *(this->m_p_age) = age;
        return this;
    }

    std::string_view get_name() {
        return m_name;
    }

    std::string_view get_breed() {
        return m_breed;
    }

    unsigned int get_age() {
        return *m_p_age;
    }

    void print_dog() const;
    ~dog_t();

private:
    std::string m_name {""};
    std::string m_breed {""};
    unsigned int* m_p_age {nullptr}; 
};


dog_t::dog_t(std::string_view name, std::string_view breed, unsigned int age):
    m_name(name), m_breed(breed), m_p_age(new unsigned int(age)) {}

void dog_t::print_dog() const {
    std::cout << "Name: " << m_name << '\n';
    std::cout << "Breed: " << m_breed << '\n';
    std::cout << "Age: " << *m_p_age << '\n';
}

dog_t::~dog_t() {
    delete m_p_age;
}


int main(int argc, char* argv[]) {
  
    dog_t const d1 {"John", "Titi", 3};
    // d1.set_age(8);

    dog_t const* d_ptr {&d1};
    dog_t const& d_ref {d1};

    d_ptr->print_dog();
    d_ref.print_dog();

    return EXIT_SUCCESS;
}
