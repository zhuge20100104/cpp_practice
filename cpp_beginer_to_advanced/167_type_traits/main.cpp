﻿#include <iostream>
#include <type_traits>

template <class T>
void is_integral_num(T num) {
    static_assert(std::is_integral_v<T>, "You should pass an integer!");
    std::cout << "You passed: " << num << '\n';
}
 
int main(int argc, char* argv[]) {
    std::cout << std::boolalpha;
    std::cout << "std::is_integral<int>::value: " << std::is_integral<int>::value << '\n';

    std::cout << "std::is_integral_v<float>: " << std::is_integral_v<float> << '\n';
    std::cout << "std::is_integral_v<char>: " << std::is_integral_v<char> << '\n';
    std::cout << "std::is_integral_v<long long int>: " << std::is_integral_v<long long int> << '\n';

    is_integral_num(21202);

    auto sum {
        []<class T>(T a, T b) {
            static_assert(std::is_floating_point_v<T>, "Please pass a float");
            return a + b;
        }
    };

    std::cout << sum(9.1, 29.6) << '\n';

    return EXIT_SUCCESS;
}
