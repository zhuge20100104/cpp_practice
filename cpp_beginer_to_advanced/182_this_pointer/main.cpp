﻿#include <iostream>
#include <concepts>
#include <type_traits>
#include <memory>
#include <string_view>


class dog_t {
public:

    dog_t() = default;
    dog_t(std::string_view name, std::string_view breed, int age);


    dog_t* set_name(std::string_view name) {
        this->m_name = name;
        return this;
    }

    dog_t* set_breed(std::string_view breed) {
        this->m_breed = breed;
        return this;
    }

    dog_t* set_age(int age) {
        *(this->m_p_age) = age;

        return this;
    }

    void print_dog();

    ~dog_t();

private:
    std::string m_name {};
    std::string m_breed {};
    int* m_p_age {nullptr};
};


dog_t::dog_t(std::string_view name, std::string_view breed, int age):
    m_name(name), m_breed(breed), m_p_age(new int(age)) {
    
    std::cout << this << " has been created!\n"; 
}

dog_t::~dog_t() {
    delete m_p_age;
    m_p_age = nullptr;

    std::cout << this << " has been killed\n";
}

void dog_t::print_dog() {
    std::cout << "Name: " << m_name << '\n';
    std::cout << "Breed: " << m_breed << '\n';
    std::cout << "Age: " << *m_p_age << '\n';
}


// Next one: 26.10_struct.cpp
int main(int argc, char* argv[]) {
  
    dog_t d1 {"Tom", "Desi", 2};
    d1.print_dog();

    d1.set_name("Gom")->set_breed("Messi")->set_age(5);

    d1.print_dog();

    return EXIT_SUCCESS;
}
