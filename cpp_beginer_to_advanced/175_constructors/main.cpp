﻿#include <iostream>
#include <concepts>
#include <type_traits>


double const c_pi = 3.14;

class cylinder_t {

public:
    cylinder_t() {
        m_base_radius = 2.0;
        m_height = 2.0;    
    }

    cylinder_t(double base_radius, double height):
        m_base_radius(base_radius), m_height(height)
     {}

    double volume() {
        return c_pi * m_base_radius * m_base_radius * m_height;
    }

private:
    double m_base_radius{};
    double m_height {};
};

int main(int argc, char* argv[]) {
    cylinder_t c1 {};
    std::cout << c1.volume() << '\n';

    cylinder_t c2 {3.2, 4.9};

    std::cout << c2.volume() << '\n';        

    return EXIT_SUCCESS;
}
