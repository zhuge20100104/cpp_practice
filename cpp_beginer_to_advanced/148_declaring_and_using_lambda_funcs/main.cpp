﻿#include <iostream>

int main(int argc, char* argv[]) {
    []() {
        std::cout << "Hello\n";
    };

    // 不具名的lambda函数，直接调用
    []() {
        std::cout << "Hello\n";
    }();

    // 参数列表可以省掉，当没有参数的时候
    auto print_hello {
        []{
            std::cout << "hello\n";
        }
    };

    print_hello();

    // 带参数的lambda函数，直接调用
    [](int a, int b) {
        std::cout << a + b << '\n';
    } (2, 10);


    auto double_sum {
        [](double a, double b) {
            return a + b;
        }
    };

    std::cout << double_sum(2.1, 2.2) << '\n';
    std::cout << double_sum(3.2, 4.8) << '\n';

    std::cout << [] (double a, double b) {
        return a + b;
    }(22, 1) << '\n';

    // 显示指定lambda的返回值，隐式转换double到int类型
    auto force_int {[](double a, double b) -> int {
        return a + b;
    }};

    std::cout << force_int(2.2, 2.2) << '\n';
    std::cout << sizeof(force_int(2.2, 2.2)) << '\n';
    
    return EXIT_SUCCESS;
}