﻿#include <iostream>
#include <concepts>
#include <type_traits>
#include <memory>
#include <string_view>


struct dog_struct_t {
    dog_struct_t() = default;
    ~dog_struct_t();

private:
    std::string m_name{};
    std::string m_breed{};
    int* m_p_age {new int(0)};
};


dog_struct_t::~dog_struct_t() {
    delete m_p_age;
    m_p_age = nullptr;
}


// Next one: 26.10_struct.cpp
int main(int argc, char* argv[]) {
  
    dog_struct_t dog;    
    return EXIT_SUCCESS;
}
