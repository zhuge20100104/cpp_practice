﻿#include <iostream>
#include <concepts>
#include <type_traits>
#include <memory>
#include <string_view>

/*
 * size of objects : generally the member variables are counted for the size of an object
 *
 * boundary alignment : Boundary alignment, also known as data alignment or memory alignment,
 * refers to the practice of aligning data in memory to specific addresses that are multiples
 * of a particular value, usually the size of the data type. The purpose of alignment is to
 * optimize memory access and improve performance, particularly on architectures that have
 * alignment requirements.
 */


 // Memory alignment
class size_demo_t {
public:
    int integer {};  // 8 
    double floating_point {}; // 12
    char character {'c'};   // 4
};


// Next one: 27.1_const.objects.cpp
int main(int argc, char* argv[]) {
  
    size_demo_t sz;
    
    std::cout << sizeof(sz) << '\n';
    std::cout << &sz.integer << '\n';
    std::cout << &sz.floating_point << '\n';
    int i_ch = (int)sz.character;
    std::cout << &i_ch << '\n';

    return EXIT_SUCCESS;
}
