﻿#include <iostream>
#include <concepts>
#include <type_traits>
#include <memory>
#include <string_view>

struct point_t {
    double x {};
    double y {};
};


void print_point(point_t const& point) {
    std::cout << "x: " << point.x << '\n';
    std::cout << "y: " << point.y << '\n';
}

// Next one: 28.1 default parameters for constructors
int main(int argc, char* argv[]) {
  
    auto [a, b] {point_t{}};

    std::cout << a << ' ' << b << '\n';

    // initial point 
    point_t p {2.2, 8.6};

    auto& [x, y] {p};

    p.x = 0.1;
    p.y = 0.9;

    std::cout << x << ' ' << y << '\n';

    print_point(p);

    auto func {[x] () {
        std::cout << "x in lambda: " << x << '\n';
    }};

    func();

    return EXIT_SUCCESS;
}
