﻿cmake_minimum_required(VERSION 3.5)

project(191_structured_bindings LANGUAGES CXX)

set(CMAKE_CXX_STANDARD 20)
set(CMAKE_CXX_STANDARD_REQUIRED ON)

add_definitions(-g)

add_executable(191_structured_bindings
    main.cpp)

target_include_directories(191_structured_bindings PRIVATE
    ${CMAKE_CURRENT_SOURCE_DIR}/beans    
    )

include(GNUInstallDirs)
install(TARGETS 191_structured_bindings
    LIBRARY DESTINATION ${CMAKE_INSTALL_LIBDIR}
    RUNTIME DESTINATION ${CMAKE_INSTALL_BINDIR}
)
