﻿#include <iostream>


// Next chapter: 21.3 Capture All in Context.cpp
int main(int argc, char* argv[]) {
    int x {1};
    int y {1};

    // 值捕获x和y， 两者在lambda函数内都是copy
    auto capture_list {
        [x, y] () {
            std::cout << "Inner x + y: " << x+y << '\n';
        }
    };

    for(std::size_t i=0; i<5; ++i) {
        capture_list();
        std::cout << "Outer x + y: " << x + y << '\n';
        ++x; 
        ++y;
    }


    std::cout << '\n';

    // 引用捕获
    auto capture_list_ref {
        [&x, &y] () {
            std::cout << "Inner x + y: " << x + y << '\n';
        }
    };

    for(std::size_t i=0; i<5; ++i) {
        capture_list_ref();
        std::cout << "Outer x + y: " << x + y << '\n';
        ++x;
        ++y;
    }


    return EXIT_SUCCESS;
}