#ifndef _FREDRIC_COMPARE_H_
#define _FREDRIC_COMPARE_H_

int max(int a, int b);
int min(int a, int b);

#endif