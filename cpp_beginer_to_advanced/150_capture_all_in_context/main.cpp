﻿#include <iostream>

int main(int argc, char* argv[]) {
    int a {6};
    int b {8};

    auto capt_by_value {
        [=]() mutable {
            ++a;
            ++b;
        }
    };

    capt_by_value();
    std::cout << a << ' ' << b << "\n";

    auto capt_by_ref {
        [&]() {
            ++a;
            ++b;
        }
    };

    capt_by_ref();

    std::cout << a << ' ' << b << "\n";
    
    return EXIT_SUCCESS;
}