﻿#include <iostream>
#include <concepts>
#include <type_traits>


double const c_pi = 3.14;

class cylinder_t {

public:
    cylinder_t() = default;

    cylinder_t(double base_radius, double height):
        m_base_radius(base_radius), m_height(height)
     {}

    auto get_radius() {
        return m_base_radius;
    }

    void set_radius(double base_radius) {
        m_base_radius = base_radius;
    }

    auto get_height() {
        return m_height;
    }

    void set_height(double height) {
        m_height = height;
    }

    double volume() {
        return c_pi * m_base_radius * m_base_radius * m_height;
    }

private:
    double m_base_radius{};
    double m_height {};
};

int main(int argc, char* argv[]) {
    cylinder_t c1 {};
    c1.set_radius(2.0);
    c1.set_height(2.0);

    std::cout << c1.get_radius() << '\n';
    std::cout << c1.get_height() << '\n';
    
    std::cout << c1.volume() << '\n';

    return EXIT_SUCCESS;
}
