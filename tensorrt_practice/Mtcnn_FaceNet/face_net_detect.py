import os
import cv2
import sys
import torch
import numpy as np
from pathlib import Path
from facenet_pytorch import InceptionResnetV1, MTCNN

# set base dir
base_dir = Path('/mnt/c/code/cpp_practice/tensorrt_practice/Mtcnn_FaceNet')
sys.path.append(str(base_dir))

class FaceEncoder:
    def __init__(self):
        # load the model when initialize
        self.model = InceptionResnetV1(pretrained='vggface2').eval()
        self.detector = MTCNN(keep_all=True).eval()
    
    def encode(self, img):
        # convert image to tensor
        if not torch.is_tensor(img):
            img = torch.tensor(img)
        if len(img.shape) == 3:
            img = img.unsqueeze(0)
        # return the feature vector of this image, 
        # can be used to do further clustering
        print(img.shape)
        return self.model(img)

    def recognize_image(self, image_path, thresh=0.7):
        # image data and image vector
        detect_dict = {}        
        for img in os.listdir(image_path):
            if img.endswith('.jpeg') or img.endswith('.jpg'):
                full_path = os.path.join(image_path, img)
                img_data = cv2.imread(full_path)
                batch_boxes, confidences = self.detector.detect(img_data)
                detect_dict[img] = []
               
                if batch_boxes is not None and len(batch_boxes) > 0:
                    for box, confidence in zip(batch_boxes, confidences):
                        x, y, x2, y2 = [int(b) for b in box]
                        # Each box contains a face 
                        # Can extract a feature vector from the cropped image
                        cropped = img_data[y:y2, x: x2]
                        cropped = cv2.cvtColor(cropped, cv2.COLOR_BGR2RGB)
                        cropped = cropped.astype(np.float32) / 255.0
                        cropped = np.transpose(cropped, (2, 0, 1))
                        feature_vector = self.encode(cropped)
                        detect_dict[img].append(feature_vector)
                        
                        # Visualize the image
                        cv2.rectangle(img_data, (x, y), (x2, y2), (0, 255, 0), 2)
                        cv2.putText(img_data, f'Detected: score{confidence}', (x, y-10), cv2.FONT_HERSHEY_DUPLEX, 0.5, (255, 255, 255), 1)
                
                cv2.imshow("output", img_data)
                while True:
                    if cv2.waitKey(100) == ord('q'):
                        cv2.destroyAllWindows()
                        break
        return detect_dict


def main():
    face_encoder = FaceEncoder()
    image_path = '/mnt/c/code/cpp_practice/tensorrt_practice/Mtcnn_FaceNet/Images'
    recognized_result = face_encoder.recognize_image(image_path)
    # print(recognized_result)
    print(recognized_result["1.jpg"][0].detach().numpy().shape)

if __name__ == '__main__':
    main()                        