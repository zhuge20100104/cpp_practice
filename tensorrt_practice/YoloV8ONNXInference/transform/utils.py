import numpy as np
import cv2
import urllib.request

# 每个class_name随机生成一种颜色
'''
    这几行代码的作用是为每个类别（`class_names` 列表中的每个元素）生成一个随机的颜色值。具体解释如下：

    1. `rng = np.random.default_rng(3)`：
    - 使用 `numpy` 的随机数生成器（`default_rng`），并为其设置了种子 `3`，这样可以确保每次运行时生成的随机数相同，保证了可重复性。

    2. `colors = rng.uniform(0, 255, size=(len(class_names), 3))`：
    - `uniform(0, 255, size=(len(class_names), 3))` 生成一个形状为 `(len(class_names), 3)` 的数组，其中 `len(class_names)` 是类别的数量（在这里是 80），`3` 表示每个颜色是由三个值组成，通常分别对应 RGB 三个通道的颜色值。
    - 生成的每个 RGB 颜色值是从均匀分布中随机抽取的，范围在 `0` 到 `255` 之间。
    
    所以，`colors` 是一个形状为 `(80, 3)` 的数组，表示 80 个类别，每个类别有一个 RGB 颜色。

    总结：
    - `colors` 的大小是 `(80, 3)`，每一行是一个 RGB 颜色的随机值。
'''

class_names = ['person', 'bicycle', 'car', 'motorcycle', 'airplane', 'bus', 'train', 'truck', 'boat', 'traffic light',
               'fire hydrant', 'stop sign', 'parking meter', 'bench', 'bird', 'cat', 'dog', 'horse', 'sheep', 'cow',
               'elephant', 'bear', 'zebra', 'giraffe', 'backpack', 'umbrella', 'handbag', 'tie', 'suitcase', 'frisbee',
               'skis', 'snowboard', 'sports ball', 'kite', 'baseball bat', 'baseball glove', 'skateboard', 'surfboard',
               'tennis racket', 'bottle', 'wine glass', 'cup', 'fork', 'knife', 'spoon', 'bowl', 'banana', 'apple',
               'sandwich', 'orange', 'broccoli', 'carrot', 'hot dog', 'pizza', 'donut', 'cake', 'chair', 'couch',
               'potted plant', 'bed', 'dining table', 'toilet', 'tv', 'laptop', 'mouse', 'remote', 'keyboard',
               'cell phone', 'microwave', 'oven', 'toaster', 'sink', 'refrigerator', 'book', 'clock', 'vase',
               'scissors', 'teddy bear', 'hair drier', 'toothbrush']


# Create a list of colors for each class where each color is a tuple of 3 integer values
rng = np.random.default_rng(3)
colors = rng.uniform(0, 255, size=(len(class_names), 3))


def read_image(img_path):
    image = cv2.imread(img_path)  # IMREAD_COLOR 确保加载为 BGR
    return image


'''
    `y[..., 0]` 和 `y[:, 0]` 不是一回事儿，它们的含义是不同的。

    ---

    ### 1. **`y[..., 0]`**
    - **`...` (省略号)** 是一个 NumPy 特殊的切片符号，表示匹配任意数量的维度。
    - 它的作用是：无论数组的维度是多少，都取最后一个维度的第 0 列（如果存在）。
    
    #### 例子 1: 对 2D 数组的操作
    ```python
    import numpy as np
    y = np.array([[1, 2], [3, 4], [5, 6]])  # shape (3, 2)
    print(y[..., 0])  # 等价于 y[:, 0]
    # 输出: [1, 3, 5]
    ```

    #### 例子 2: 对 3D 数组的操作
    ```python
    y = np.array([
        [[1, 2], [3, 4]],
        [[5, 6], [7, 8]]
    ])  # shape (2, 2, 2)
    print(y[..., 0])  # 获取最后一维的第 0 列
    # 输出:
    # [[1 3]
    #  [5 7]]
    ```

    #### 总结
    - **适用维度任意的数组**。
    - 在高维数组中，可以更方便地获取 **最后一维第 0 列**。

    ---

    ### 2. **`y[:, 0]`**
    - **`:` (冒号)** 表示在这一维中选择所有元素。
    - **`[:, 0]`** 明确要求 **对第 1 个维度操作（即第 0 轴之后的一维）**，取其第 0 列。

    #### 例子 1: 对 2D 数组的操作
    ```python
    y = np.array([[1, 2], [3, 4], [5, 6]])  # shape (3, 2)
    print(y[:, 0])  # 等价于 y[..., 0]
    # 输出: [1, 3, 5]
    ```

    #### 例子 2: 对 3D 数组的操作
    ```python
    y = np.array([
        [[1, 2], [3, 4]],
        [[5, 6], [7, 8]]
    ])  # shape (2, 2, 2)
    print(y[:, 0])  # 获取第 1 个维度（即 2x2x2 中的 "2x2"）的第 0 行
    # 输出:
    # [[1 2]
    #  [5 6]]
    ```

    #### 总结
    - **只能用于二维或更高维数组的第 1 轴**。
    - 如果数组维度不同，比如 1D 或更高维度时使用不当会出错。

    ---

    ### 3. **区别总结**
    | 特性                  | `y[..., 0]`                      | `y[:, 0]`                 |
    |-----------------------|----------------------------------|--------------------------|
    | **适用维度**          | 任意维度                         | 至少 2 维                 |
    | **作用的轴**          | 最后一维                         | 第二维                   |
    | **含义**              | 获取最后一维的第 0 列            | 获取第 1 轴的第 0 行       |
    | **灵活性**            | 更灵活，可用于多维数组            | 仅适用于二维及更高维数组   |

    ---

    ### 4. **具体代码的例子**
    #### 示例数组
    ```python
    x = np.array([
        [[1, 2, 3], [4, 5, 6]],
        [[7, 8, 9], [10, 11, 12]]
    ])  # shape (2, 2, 3)
    ```

    #### 使用 `...`
    ```python
    y = np.zeros_like(x)
    y[..., 0] = x[..., 0] - x[..., 2] / 2
    # `x[..., 0]` 取最后一维的第 0 列: [[1, 4], [7, 10]]
    # `x[..., 2]` 取最后一维的第 2 列: [[3, 6], [9, 12]]
    # 计算结果: [[1 - 3/2, 4 - 6/2], [7 - 9/2, 10 - 12/2]]
    # 输出:
    # [[[ -0.5,  0.0,  0.0],
    #   [  1.0,  0.0,  0.0]],
    #  [[  2.5,  0.0,  0.0],
    #   [  4.0,  0.0,  0.0]]]
    ```

    #### 使用 `[:, 0]`
    ```python
    y = np.zeros_like(x)
    y[:, 0] = x[:, 0] - x[:, 1]
    # `x[:, 0]` 取第 1 轴（即 2x3 的矩阵）中的第 0 行: [[1, 2, 3], [7, 8, 9]]
    # `x[:, 1]` 取第 1 轴中的第 1 行: [[4, 5, 6], [10, 11, 12]]
    # 计算结果: [[[1-4, 2-5, 3-6], ...]]
    # 输出:
    # [[[ -3,  -3,  -3],
    #   [  0,   0,   0]],
    #  [[ -3,  -3,  -3],
    #   [  0,   0,   0]]]
    ```

    ---

    希望这清楚地区分了 `y[..., 0]` 和 `y[:, 0]` 的用途和差别！如果还有疑问，随时提问~ 😊
'''
def xywh2xyxy(x):
    # Convert bounding box (x, y, w, h) to bounding box (x1, y1, x2, y2)
    # top lef bottom corner
    y = np.copy(x)
    y[..., 0] = x[..., 0] - x[..., 2] /2
    y[..., 1] = x[..., 1] - x[..., 3] /2
    y[..., 2] = x[..., 0] + x[..., 2] /2
    y[..., 3] = x[..., 1] + x[..., 3] /2
    return y


def compute_iou(box, boxes):
    # Compute xmin, ymin, xmax, ymax for both boxes
    xmin = np.maximum(box[0], boxes[:, 0])
    ymin = np.maximum(box[1], boxes[:, 1])
    xmax = np.minimum(box[2], boxes[:, 2])
    ymax = np.minimum(box[3], boxes[:, 3])

    # Compute the intersection area
    intersection_area = np.maximum(0, xmax-xmin) * np.maximum(0, ymax - ymin)

    # Compute the union area
    box_area = (box[2] - box[0]) * (box[3] - box[1])
    boxes_area = (boxes[:, 2] - boxes[:, 0]) * (boxes[:, 3] - boxes[:, 1])
    union_area = box_area + boxes_area - intersection_area

    # compute IoU
    iou = intersection_area/ union_area
    return iou


def nms(boxes, scores, iou_threshold):
    # sort by score
    sorted_indices = np.argsort(scores)[::-1]

    keep_boxes = []

    while sorted_indices.size > 0:
        # Pick the last box
        box_id = sorted_indices[0]

        keep_boxes.append(box_id)
        # Compute IoU of the picked box with the rest
        ious = compute_iou(boxes[box_id, :], boxes[sorted_indices[1:], :])

        # Remove boxes with IoU over the threshold
        keep_indices = np.where(ious < iou_threshold)[0]
        # 因为keep_indices是从第一个开始算起的，
        # 第一个元素被刨除了，他是最上面的参考系，他的confidence_score最大，需要保留
        sorted_indices = sorted_indices[keep_indices + 1]
    return keep_boxes

def sigmoid(x):
    return 1/ (1 + np.exp(-x))


def draw_masks(image, boxes, class_ids, mask_alpha=0.3, mask_maps=None):
    mask_img = image.copy()
    for i, (box, class_id) in enumerate(zip(boxes, class_ids)):
        color = colors[class_id]
        x1, y1, x2, y2 = box.astype(int)

        if mask_maps is None:
            cv2.rectangle(mask_img, (x1, y1), (x2, y2), color, -1)
        else:
            crop_mask = mask_maps[i][y1:y2, x1:x2, np.newaxis]
            crop_mask_img = mask_img[y1:y2, x1:x2]
            crop_mask_img = crop_mask_img * (1-crop_mask) + crop_mask * color
            mask_img[y1:y2, x1:x2] = crop_mask_img
    
    return cv2.addWeighted(mask_img, mask_alpha, image, 1-mask_alpha, 0)


def draw_detections(image, boxes, scores, class_ids, mask_alpha=0.3, mask_maps=None):
    img_height, img_width = image.shape[:2]
    size = min([img_height, img_width]) * 0.0006
    text_thickness = int(min([img_height, img_width])*0.001)

    mask_img = draw_masks(image, boxes, class_ids, mask_alpha, mask_maps)

    # Draw bounding boxes and labels of detections
    for box, score, class_id in zip(boxes, scores, class_ids):
        color = colors[class_id]
        x1, y1, x2, y2 = box.astype(int)
        
        # Draw rectangle
        cv2.rectangle(mask_img, (x1, y1), (x2, y2), color, 2)

        label = class_names[class_id]

        caption = f"{label} {int(score*100)}%"
        (tw, th), _ = cv2.getTextSize(text=caption, fontFace=cv2.FONT_HERSHEY_SIMPLEX, 
                                      fontScale=size, thickness=text_thickness)
        th = int(th * 1.2)

        cv2.rectangle(mask_img, (x1, y1), 
                    (x1 + tw, y1 - th), color, -1)

        cv2.putText(mask_img, caption, (x1, y1), cv2.FONT_HERSHEY_SIMPLEX, size, (255, 255, 255),
                    text_thickness, cv2.LINE_AA)
    return mask_img
