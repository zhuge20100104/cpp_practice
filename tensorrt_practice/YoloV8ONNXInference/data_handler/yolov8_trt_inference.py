# 推理代码
'''

    在这个代码中，`outputs0` 和 `outputs1` 是从设备（GPU）复制回主机（CPU）的数据，`np.ascontiguousarray()` 主要是确保 NumPy 数组在内存中是连续存储的，以便进行高效的内存操作和避免潜在的内存布局问题。

    ### 回答你的问题：
    - **是否需要对输出数据使用 `np.ascontiguousarray()`？**

    一般来说，**输出数组（`outputs0` 和 `outputs1`）** 是否需要使用 `np.ascontiguousarray()` 取决于你将输出从设备内存复制到主机内存时的要求。大部分情况下，`outputs0` 和 `outputs1` 在你创建时已经是 NumPy 数组，且通常是连续的，因此你不一定需要再次调用 `ascontiguousarray()`。

    #### 理由：
    1. **`cuda.memcpy_dtoh()`**：这个函数直接将数据从 GPU 内存复制到 CPU 内存，不会影响 NumPy 数组的内存布局，假如 `outputs0` 和 `outputs1` 在创建时已经是正常的 NumPy 数组，它们应该已经满足内存要求。
    
    2. **`np.empty()`** 创建的 NumPy 数组默认是连续的，因此除非你有特定要求，否则不必再次调用 `ascontiguousarray()`。

    3. **`ascontiguousarray()`** 更常用的场景是，当你需要确保某个数组是连续存储，或者从非连续数组（比如某些切片操作后的数组）构建新的数组时，才需要调用它。

    ### 结论：
    - **不需要**在输出数组上调用 `np.ascontiguousarray()`，因为你使用 `np.empty()` 创建的数组是连续的，且 `cuda.memcpy_dtoh()` 已经会正确处理数据复制。
    - 你可以直接使用 `outputs0` 和 `outputs1`，而不必担心内存布局问题。

    ### 最终代码修改：

    ```python
    import pycuda.driver as cuda
    import pycuda.autoinit
    import numpy as np
    import time

    # 假设 inputs 是输入数据，full_img_paths 是图像路径
    for inputs, full_img_path in zip(input_list, full_img_paths):
        # 记录开始时间
        self.start = time.time()

        # 确保数据是连续的
        inputs = np.ascontiguousarray(inputs)

        # 假设 outputs 是模型输出的形状
        outputs0_shape = (1, 80, 80, 80)  # 示例形状
        outputs1_shape = (1, 40, 40, 40)  # 示例形状
        outputs0 = np.empty(outputs0_shape, dtype=np.float32)
        outputs1 = np.empty(outputs1_shape, dtype=np.float32)

        # 分配设备内存
        d_inputs = cuda.mem_alloc(inputs.nbytes)
        d_output0 = cuda.mem_alloc(outputs0.nbytes)
        d_output1 = cuda.mem_alloc(outputs1.nbytes)

        # 绑定输入和输出
        bindings = [d_inputs, d_output0, d_output1]

        # 将输入数据从主机内存复制到设备内存
        cuda.memcpy_htod(d_inputs, inputs)

        # 执行推理
        self.context.execute_v2(bindings)

        # 将输出数据从设备内存复制到主机内存
        cuda.memcpy_dtoh(outputs0, d_output0)
        cuda.memcpy_dtoh(outputs1, d_output1)

        # 释放设备内存
        d_inputs.free()
        d_output0.free()
        d_output1.free()

        # 记录结束时间
        self.end = time.time()
        print(f"Inference time: {self.end - self.start} seconds")
    ```

    这段代码中，`np.empty()` 已经创建了连续数组，因此直接使用它们即可。

'''
import math
import tensorrt as trt
import pycuda.autoinit
import pycuda.driver as cuda
import cv2
import numpy as np
import os 
from PIL import Image
import matplotlib.pyplot as plt
import yaml
import time 

from pathlib import Path
import sys

base_dir = Path("/mnt/c/code/cpp_practice/tensorrt_practice/YoloV8ONNXInference")
sys.path.append(str(base_dir))

from transform.utils import xywh2xyxy, nms, draw_detections, sigmoid, read_image


class TRTInference:

    # specify engine file path and input and output shape
    def __init__(self, engine_file_path, input_shape, output_shape0, output_shape1, class_labels_file, num_mask, conf_threshold, score_threshold, nms_threshold):
        self.logger = trt.Logger(trt.Logger.WARNING)
        self.engine_file_path = engine_file_path

        # load engine here
        self.engine = self.load_engine(self.engine_file_path)

        # create context
        self.context = self.engine.create_execution_context()

        self.num_mask = num_mask
        self.conf_thres = conf_threshold
        self.score_threshold = score_threshold
        self.ios_thres = nms_threshold

        # input shape 
        self.input_shape = input_shape
        self.class_labels_file = class_labels_file
        self.count = 0

        # output shape
        self.output_shape0 = output_shape0
        self.output_shape1 = output_shape1
        self.input_height = 640
        self.input_width = 640

        # 输出IO Tensor的name和shape
        for i in range(self.engine.num_io_tensors):
            tensor_name = self.engine.get_tensor_name(i)
            tensor_shape = self.engine.get_tensor_shape(tensor_name)
            print(f"IOTensors: {i}: {tensor_name}, Shape: {tensor_shape}")

        with open(class_labels_file, 'r') as class_read:
            data = yaml.safe_load(class_read)
            self.class_labels = [name for name in data['names'].values()]

    def load_engine(self, engine_file_path):
        with open(engine_file_path, 'rb') as f:
            runtime = trt.Runtime(self.logger)
            engine_deserialized = runtime.deserialize_cuda_engine(f.read())
            return engine_deserialized
    
    def preprocess_image(self, image_path):
        img_list = []
        img_path = []
        img_size_list = [] 

        count = 0

        for img_original in os.listdir(image_path):
            img_full_path = os.path.join(image_path, img_original)
            self.img = cv2.imread(img_full_path)
            self.img = cv2.cvtColor(self.img, cv2.COLOR_BGR2RGB)
            self.org_h, self.org_w = self.img.shape[:2]
            self.img_resized = cv2.resize(self.img, (self.input_shape[2], self.input_shape[3]), interpolation=cv2.INTER_AREA)
            img_np = np.array(self.img_resized).astype(np.float32)/ 255.0
            img_np = img_np.transpose((2, 0, 1))
            img_np = np.expand_dims(img_np, axis=0)
            self.resized_imgH, self.resized_imgW = self.img_resized.shape[:2]
            count += 1
            img_size_list.append((self.org_h, self.org_w))
            img_list.append(img_np)
            img_path.append(img_full_path)

            # Only select 12 pictures
            if count >= 12:
                continue
        return img_size_list, img_list, img_path

    @staticmethod
    def rescale_boxes(boxes, input_shape, image_shape):
        
        # scale factor
        scale_x = image_shape[1]/ input_shape[1]
        scale_y = image_shape[0]/ input_shape[0]

        # rescale boxes 
        rescaled_boxes = boxes.copy()

        # rescale xmin, ymin, xmax, ymax
        rescaled_boxes[:, 0] = boxes[:, 0] * scale_x
        rescaled_boxes[:, 1] = boxes[:, 1] * scale_y
        rescaled_boxes[:, 2] = boxes[:, 2] * scale_x
        rescaled_boxes[:, 3] = boxes[:, 3] * scale_y

        return rescaled_boxes

    def extract_boxes(self, box_predictions):
        boxes = box_predictions[:, :4]
        print(f'box shape: {boxes.shape}')
        boxes = self.rescale_boxes(boxes, 
                                   (self.input_height, self.input_width),
                                   (self.img_height, self.img_width))
        # convert boxes to xyxy format

        boxes = xywh2xyxy(boxes)

        boxes[:, 0] = np.clip(boxes[:, 0], 0, self.img_width)
        boxes[:, 1] = np.clip(boxes[:, 1], 0, self.img_height)
        boxes[:, 2] = np.clip(boxes[:, 2], 0, self.img_width)
        boxes[:, 3] = np.clip(boxes[:, 3], 0, self.img_height)

        return boxes

    def process_box_output(self, box_output):
        # take output 0 , and preprocess on 1, 116, 8400 
        # remove 1 and transpose to 8400, 116
        predictions = np.squeeze(box_output).T
        # num of mask 32 for yolov8

        print(predictions.shape)

        num_classes = box_output.shape[1] - self.num_mask - 4
        # filtering out object scores and confidense,threshold

        # 8400, 1
        scores = np.max(predictions[:, 4:4+num_classes], axis=1)
        # scores from 8400,  (N,  116)
        predictions = predictions[scores > self.conf_thres, :]
        # N, 1
        scores = scores[scores > self.conf_thres]

        if len(scores) == 0:
            return [], [], [], np.array([])

        box_predictions = predictions[..., :num_classes+4]
        mask_predictions = predictions[..., num_classes+4:]

        class_ids = np.argmax(box_predictions[:, 4:], axis=1)
        # get bounding for each object
        boxes = self.extract_boxes(box_predictions)
        # apply non max supression
        indices = nms(boxes, scores, self.ios_thres)

        return boxes[indices], scores[indices], class_ids[indices], mask_predictions[indices]

    def process_mask_output(self, mask_predictions, mask_output):
        if mask_predictions.shape[0] == 0:
            return []
        
        mask_output = np.squeeze(mask_output)
        
        # calculate mask maps for each box
        
        num_mask, mask_height, mask_width = mask_output.shape # CHW
        
        
        masks = sigmoid(mask_predictions @ mask_output.reshape(num_mask, -1))
        
        masks = masks.reshape((-1, mask_height, mask_width))
        
        # downscale boxes to match mask size
        
        scale_boxes = self.rescale_boxes(self.boxes,
                                         (self.img_height, self.img_width),
                                         (mask_height, mask_width))
        
        # get mask map for every box / mask pair
        
        mask_maps = np.zeros((len(scale_boxes), self.img_height, self.img_width))
        blur_size = (int(int(self.img_width) / mask_width), int(int(self.img_height) / mask_height))
        
        for i in range(len(scale_boxes)):
            
            scale_x1 = int(math.floor(scale_boxes[i][0]))
            scale_y1 = int(math.floor(scale_boxes[i][1]))
            scale_x2 = int(math.ceil(scale_boxes[i][2]))
            scale_y2 = int(math.ceil(scale_boxes[i][3]))
            
            x1 = int(math.floor(self.boxes[i][0]))
            y1 = int(math.floor(self.boxes[i][1]))
            x2 = int(math.ceil(self.boxes[i][2]))
            y2 = int(math.ceil(self.boxes[i][3]))
            
            
            scale_crop_mask = masks[i][scale_y1:scale_y2, scale_x1:scale_x2]
            
            crop_mask = cv2.resize(scale_crop_mask,
                                   (x2-x1, y2-y1),
                                   interpolation=cv2.INTER_CUBIC)
            
            crop_mask = cv2.blur(crop_mask, blur_size)
            
            crop_mask = (crop_mask > 0.5).astype(np.uint8)
            mask_maps[i, y1:y2, x1:x2] = crop_mask
            
        return mask_maps    



    def inference_detection(self, image_path):
        img_size_list, input_list, full_img_paths = self.preprocess_image(image_path)
        
        self.total_time = 0
        self.num_frames = len(input_list)
              
        for img_size, inputs, full_img_path in zip(img_size_list, input_list, full_img_paths):
            self.img_height, self.img_width = img_size

            print(f'self.img_height, self.img_width: {self.img_height}, {self.img_width}')
            # start time
            self.start = time.time()

            print(f'inputs.shape: {inputs.shape}')
            inputs = np.ascontiguousarray(inputs)

            outputs0 = np.empty(self.output_shape0, dtype=np.float32)
            outputs1 = np.empty(self.output_shape1, dtype=np.float32)

            d_inputs = cuda.mem_alloc(1 * inputs.nbytes)
            d_outputs0 = cuda.mem_alloc(1 * outputs0.nbytes)
            d_outputs1 = cuda.mem_alloc(1 * outputs1.nbytes)

            bindings = [d_inputs, d_outputs0, d_outputs1]
            
            cuda.memcpy_htod(d_inputs, inputs)

            self.context.execute_v2(bindings)

            cuda.memcpy_dtoh(outputs0, d_outputs0)
            cuda.memcpy_dtoh(outputs1, d_outputs1)

            d_inputs.free()
            d_outputs0.free()
            d_outputs1.free()

            self.end = time.time()

            self.total_time += (self.end - self.start)

            self.fps = self.num_frames / self.total_time
            
            self.boxes, self.scores, self.class_ids, mask_pred = self.process_box_output(outputs0)
            # create map
            self.mask_maps = self.process_mask_output(mask_pred, outputs1)
            
            image = cv2.imread(full_img_path)
            combined_image = draw_detections(image, self.boxes, self.scores, self.class_ids,
                               0.5, mask_maps=self.mask_maps)
            
            cv2.namedWindow('Detect&Segment', cv2.WINDOW_NORMAL)
            cv2.resizeWindow('Detect&Segment', 1000, 720)
            cv2.imshow('Detect&Segment', combined_image)

            cv2.waitKey(8000)
        
        cv2.destroyAllWindows() 
        


def main():
    engine_file_path ='/mnt/c/code/cpp_practice/tensorrt_practice/YoloV8ONNXInference/data/input/yolov8m-seg.engine'

    # Load the TensorRT engine
    input_shape = (1,3, 640, 640)
    output_shape0 = (1,116,8400)
    output_shape1 = (1,32,160,160)

    image_path = '/mnt/c/code/cpp_practice/tensorrt_practice/YoloV8ONNXInference/data/input/Images'

    path_to_class = "/mnt/c/code/cpp_practice/tensorrt_practice/YoloV8ONNXInference/data/input/coco.yaml"

    inference = TRTInference(engine_file_path, input_shape, output_shape0, output_shape1, path_to_class, 32, 0.38, 0.3, 0.3)

    inference.inference_detection(image_path)

if __name__ == '__main__':
    main()
        