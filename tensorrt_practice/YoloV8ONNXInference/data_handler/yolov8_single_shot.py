# preprocessing import file

import cv2
import sys
from pathlib import Path
import argparse
import os
from ultralytics import YOLO

base_dir = Path('/mnt/c/code/cpp_practice/tensorrt_practice/YoloV8ONNXInference')

sys.path.append(str(base_dir))

# class declare

class YoloV8SingleShot:

    # model path
    def __init__(self, images_path, output_directory):
        self.images_path = images_path
        self.output_directory = output_directory
        
    def inference(self):
        for images in os.listdir(self.images_path):
            img_full_path = os.path.join(self.images_path, images)
            img = cv2.imread(img_full_path)
            self.model = YOLO(base_dir/ "data/input/yolov8m-seg.pt")
            self.model.predict(source=img, conf=0.25, save=True, name=self.output_directory)


def main():
    parser = argparse.ArgumentParser(description='YoloV8 Single Shot Inference on CPU')
    parser.add_argument('--images_path', type=str, required=True, help='Path to images directory')
    parser.add_argument('--output_directory', type=str, required=True, help='Output directory parsing')

    args = parser.parse_args()
    yolov8 = YoloV8SingleShot(args.images_path, args.output_directory)
    yolov8.inference() 

if __name__ == '__main__':
    main()

