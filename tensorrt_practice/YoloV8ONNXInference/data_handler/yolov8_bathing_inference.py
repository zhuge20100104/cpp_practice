import math
import time
import cv2
import numpy as np

import onnxruntime
import sys
from pathlib import Path


# YoloV8 output的含义
'''
    一张图有若干个框，
    每个框对应一个类别，
    每个框对应一张掩码图，
    掩码图还原后的大小和原图大小一样，而且输出值在 0-1之间，代表属于当前类别的概率。

    两个输出，
    output0
    和output1的含义是，

    output0
    1, 116, 8400


    8400表示不同的尺寸的瞄点框共输出 8400个结果

    116列

    前4列是 (centerx, centery, width, height)
    中间80列是  confidence score
    最后32列是 掩码权重


    output1
    1, 32, 160, 160

    output1中的32是掩码权重，
    与 116列中的最后32列进行 matmul之后，得到
    N, 160, 160大小的掩码图像
    掩码图像 缩放回原图大小，
    平滑过后，二值化（过sigmoid函数)，
    得到当前box中的属于当前类别的概率
'''

# 掩码框的含义，Code TODO:
'''
    **是的**，每个框的掩码对应的类别就是当前框的 **class类别**。具体解释如下：

    ---

    ### **绑定关系**
    1. **框与类别绑定**：
    - 每个框（从 `output0` 的 8400 个候选框中挑选出的有效框）都通过 **中间 80 列的类别置信度**，确定一个最高置信度的类别 \( \text{class} \)。  
        例如：
        - 一个框对应的类别置信度可能是 \([0.1, 0.2, 0.05, ..., 0.9, ...]\)，最高值 \( 0.9 \) 对应某个类别，比如 "dog"。

    2. **框与掩码绑定**：
    - 每个框的 **掩码权重（最后 32 列）** 用于生成该框的掩码图（通过与基础掩码 `output1` 的线性组合）。
    - 这个掩码表示该框内的 **目标像素区域**。

    3. **掩码与类别绑定**：
    - 每个框的掩码仅表示该框对应的类别目标在框内的概率分布。
    - 例如：
        - 如果一个框被识别为 "dog"，则其对应的掩码图表示 **框内属于狗的像素概率分布**。

    ---

    ### **简化的流程**
    1. 检测框 \( \text{Box}_i \)：
    - 通过置信度（`output0` 的中间 80 列），选择类别 \( \text{Class}_i \)。

    2. 生成掩码 \( \text{Mask}_i \)：
    - 使用框的 **掩码权重（最后 32 列）** 和 **基础掩码** 计算 \( \text{Mask}_i \)。
    - 生成的掩码是针对 \( \text{Class}_i \) 的。

    3. 掩码应用：
    - 缩放掩码到原图大小，并应用到框的区域内。
    - 掩码表示 \( \text{Box}_i \) 内属于 \( \text{Class}_i \) 的像素概率分布。

    ---

    ### **结论**
    掩码对应的类别是当前框的类别（`class_id`），即掩码图只表示框中属于该类别的目标像素区域。

'''

base_dir = Path("/mnt/c/code/cpp_practice/tensorrt_practice/YoloV8ONNXInference")
sys.path.append(str(base_dir))

from transform.utils import xywh2xyxy, nms, draw_detections, sigmoid, read_image

'''

CLASS INIT
    target: YoloV8 segment
    path: Yolov8 onnx model path
    conf_thres: confidence threshold
    ios_thres: nms iou threshold
    num_mask: mask channel number
'''

class YoloSeg:
    
    def __init__(self, path, conf_thres = 0.7, ios_thres = 0.5, num_mask=32):
        self.path = path
        self.conf_thres = conf_thres
        self.ios_thres = ios_thres
        self.num_mask = num_mask

        self.initialize_model(path)
    
    def __call__(self, image):
        return self.segment_objects(image)

    def initialize_model(self, path):

        # In the example below if there is a kernel in the CUDA execution provider ONNX Runtime 
        # executes that on GPU. If not the kernel is executed on CPU.
        self.session = onnxruntime.InferenceSession(path, 
                                                    providers=[
                                                        'CUDAExecutionProvider',
                                                        'CPUExecutionProvider'
                                                    ])
        self.get_input_details()

        self.get_output_details()

    def get_input_details(self):
        model_inputs = self.session.get_inputs()
        self.input_names = [model_inputs[i].name for i in range(len(model_inputs))]

        self.input_shape = model_inputs[0].shape
        self.input_height = self.input_shape[2]
        self.input_width = self.input_shape[3]
    
    def get_output_details(self):
        model_outputs = self.session.get_outputs()
        self.output_names = [model_outputs[i].name for i in range(len(model_outputs))]

    def prepare_input(self, image):
        self.img_height, self.img_width = image.shape[:2]
        input_img = cv2.cvtColor(image, cv2.COLOR_BGR2RGB)
        input_img = cv2.resize(input_img, (self.input_width, self.input_height))
        # scale it to pixel [0,1]
        input_img = input_img /255.0
        input_img = input_img.transpose(2, 0, 1)
        # np.newaxis 用于在最前面插入 batch 维度，: 表示保留原来的所有元素，
        # 这样可以把一张图片扩展为神经网络输入的格式 [batch_size, channels, height, width]。
        input_tensor = input_img[np.newaxis, :, :, :].astype(np.float32)
        return input_tensor

    def inference(self, input_tensor):
        start = time.perf_counter()
        outputs = self.session.run(self.output_names, {self.input_names[0]: input_tensor})
        end = time.perf_counter()

        print(f'Inference time elaspe: {end - start} in seconds')
        return outputs

    @staticmethod
    def rescale_boxes(boxes, input_shape, image_shape):
        
        # scale factor
        scale_x = image_shape[1]/ input_shape[1]
        scale_y = image_shape[0]/ input_shape[0]

        # rescale boxes 
        rescaled_boxes = boxes.copy()

        print(f'')

        # rescale xmin, ymin, xmax, ymax
        rescaled_boxes[:, 0] = boxes[:, 0] * scale_x
        rescaled_boxes[:, 1] = boxes[:, 1] * scale_y
        rescaled_boxes[:, 2] = boxes[:, 2] * scale_x
        rescaled_boxes[:, 3] = boxes[:, 3] * scale_y

        return rescaled_boxes

    def extract_boxes(self, box_predictions):
        boxes = box_predictions[:, :4]

        print(f'box shape: {boxes.shape}')
        boxes = self.rescale_boxes(boxes, 
                                   (self.input_height, self.input_width),
                                   (self.img_height, self.img_width))
        # convert boxes to xyxy format

        boxes = xywh2xyxy(boxes)

        boxes[:, 0] = np.clip(boxes[:, 0], 0, self.img_width)
        boxes[:, 1] = np.clip(boxes[:, 1], 0, self.img_height)
        boxes[:, 2] = np.clip(boxes[:, 2], 0, self.img_width)
        boxes[:, 3] = np.clip(boxes[:, 3], 0, self.img_height)

        return boxes

    def process_box_output(self, box_output):
        # take output 0 , and preprocess on 1, 116, 8400 
        # remove 1 and transpose to 8400, 116
        predictions = np.squeeze(box_output).T
        # num of mask 32 for yolov8

        num_classes = box_output.shape[1] - self.num_mask - 4
        # filtering out object scores and confidense,threshold

        # 8400, 1
        scores = np.max(predictions[:, 4:4+num_classes], axis=1)
        # scores from 8400,  (N,  116)
        predictions = predictions[scores > self.conf_thres, :]
        # N, 1
        scores = scores[scores > self.conf_thres]

        if len(scores) == 0:
            return [], [], [], np.array([])

        box_predictions = predictions[..., :num_classes+4]
        mask_predictions = predictions[..., num_classes+4:]

        class_ids = np.argmax(box_predictions[:, 4:], axis=1)
        # get bounding for each object
        boxes = self.extract_boxes(box_predictions)
        # apply non max supression
        indices = nms(boxes, scores, self.ios_thres)

        return boxes[indices], scores[indices], class_ids[indices], mask_predictions[indices]

    def process_mask_output(self, mask_predictions, mask_output):
        if mask_predictions.shape[0] == 0:
            return []
        
        mask_output = np.squeeze(mask_output)
        
        # calculate mask maps for each box
        
        num_mask, mask_height, mask_width = mask_output.shape # CHW
        
        
        masks = sigmoid(mask_predictions @ mask_output.reshape(num_mask, -1))
        
        masks = masks.reshape((-1, mask_height, mask_width))
        
        # downscale boxes to match mask size
        
        scale_boxes = self.rescale_boxes(self.boxes,
                                         (self.img_height, self.img_width),
                                         (mask_height, mask_width))
        
        # get mask map for every box / mask pair
        
        mask_maps = np.zeros((len(scale_boxes), self.img_height, self.img_width))
        blur_size = (int(int(self.img_width) / mask_width), int(int(self.img_height) / mask_height))
        
        for i in range(len(scale_boxes)):
            
            scale_x1 = int(math.floor(scale_boxes[i][0]))
            scale_y1 = int(math.floor(scale_boxes[i][1]))
            scale_x2 = int(math.ceil(scale_boxes[i][2]))
            scale_y2 = int(math.ceil(scale_boxes[i][3]))
            
            x1 = int(math.floor(self.boxes[i][0]))
            y1 = int(math.floor(self.boxes[i][1]))
            x2 = int(math.ceil(self.boxes[i][2]))
            y2 = int(math.ceil(self.boxes[i][3]))
            
            
            scale_crop_mask = masks[i][scale_y1:scale_y2, scale_x1:scale_x2]
            
            crop_mask = cv2.resize(scale_crop_mask,
                                   (x2-x1, y2-y1),
                                   interpolation=cv2.INTER_CUBIC)
            
            crop_mask = cv2.blur(crop_mask, blur_size)
            
            crop_mask = (crop_mask > 0.5).astype(np.uint8)
            mask_maps[i, y1:y2, x1:x2] = crop_mask
            
        return mask_maps    


    def segment_objects(self, image):
        input_tensor = self.prepare_input(image)
        # inference 
        outputs = self.inference(input_tensor)

        print(f'outputs0 shape: {outputs[0].shape}, dtype: {outputs[0].dtype}')
        print(f'outputs1 shape: {outputs[1].shape}, dtype: {outputs[1].dtype}')

        self.boxes, self.scores, self.class_ids, mask_pred = self.process_box_output(outputs[0])

        # create map
        self.mask_maps = self.process_mask_output(mask_pred, outputs[1])

        return self.boxes, self.scores, self.class_ids, self.mask_maps

    def draw_masks(self, image, draw_scores=True, mask_alpha=0.5):
        return draw_detections(image, self.boxes, self.scores, self.class_ids,
                               mask_alpha, mask_maps=self.mask_maps)
        


def main():
    model_path = base_dir / 'data/input/yolov8m-seg.onnx'
    yolo_seg = YoloSeg(model_path, conf_thres=0.3, ios_thres=0.5)
    img_url = './data/input/street.jpg'
    img = read_image(img_url)
    yolo_seg(img)

    combined_img = yolo_seg.draw_masks(img)
    cv2.namedWindow('Detect&Segment', cv2.WINDOW_NORMAL)
    cv2.resizeWindow('Detect&Segment', 1000, 720)
    cv2.imshow('Detect&Segment', combined_img)

    while True:
        k = cv2.waitKey(30) & 0xFF
        if k == 27:
            cv2.destroyAllWindows()
            break


if __name__ == '__main__':
    main()

