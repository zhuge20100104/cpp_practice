import onnx

# 加载模型
model = onnx.load("inceptionv3.onnx")

# 验证模型是否有效
onnx.checker.check_model(model)