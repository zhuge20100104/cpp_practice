'''
    Python packages
    1. os
    2. cv2
    3. time
'''
import os 
import cv2
import pycuda.autoinit
import pycuda.driver as cuda
import tensorrt as trt
import time
import numpy as np
from tensorflow.keras.preprocessing import image
from tensorflow.keras.applications.inception_v3 import preprocess_input

class InceptionV3TFEngine:
    # Init of parameters
    def __init__(self, engine_file_path, class_labels, N_warmup_run=10, N_run=1000):
        self.logger = trt.Logger(trt.Logger.WARNING)
        self.engine_file_path = engine_file_path
        self.N_warmup_run = N_warmup_run
        self.N_run = N_run
        # load engine 
        self.engine = self.load_engine(self.engine_file_path)
        self.context = self.engine.create_execution_context()

        BATCH_SIZE=32
        # frame image
        # load images
        self.batched_inputs = self.batch_input_prepare(BATCH_SIZE)

        self.output_shape = (BATCH_SIZE, 1000)

        # read class coco files with open
        with open(class_labels, 'r') as read_classes:
            self.class_labels = [classes.strip() for classes in read_classes.readlines() ]

    
    '''
        load engine file and deserialize for an inference
        param[1]: engine_file_path
        param[out]: deserialized_engine
    '''
    def load_engine(self, engine_file_path):
        with open(engine_file_path, 'rb') as f:
            runtime = trt.Runtime(self.logger)
            engine_deserialized = runtime.deserialize_cuda_engine(f.read())
        return engine_deserialized

    def batch_input_prepare(self, batch_size = 8):
        batched_input = np.zeros((batch_size, 299, 299, 3), dtype=np.float32)

        for i in range(batch_size):

            img_path = './data/img%d.JPG' % (i % 4)

            img = image.load_img(img_path, target_size=(299, 299))
            x = image.img_to_array(img)
            x = np.expand_dims(x, axis=0)
            x = preprocess_input(x)
            batched_input[i, :] = x
        return batched_input
    
    def run_a_round_of_inference(self):
        inputs = np.ascontiguousarray(self.batched_inputs)

        input_shape = (32, 299, 299, 3)  # 根据你的模型输入尺寸调整 input_size
        # 确保设置输入张量的形状
        self.context.set_input_shape("keras_tensor", input_shape)

        outputs = np.empty(self.output_shape, dtype=np.float32)
        d_inputs = cuda.mem_alloc(1 * inputs.nbytes)
        d_outputs = cuda.mem_alloc(1 * outputs.nbytes)


        start_time = time.perf_counter()
        bindings = [d_inputs, d_outputs]
        cuda.memcpy_htod(d_inputs, inputs)
        self.context.execute_v2(bindings)
        cuda.memcpy_dtoh(outputs, d_outputs)
        end_time = time.perf_counter()

        d_inputs.free()
        d_outputs.free()
        elapsed_time = end_time - start_time
        return elapsed_time, outputs 
    
    def inference_run(self):
            
            elapsed_time = []
            # Warm up
            for i in range(self.N_warmup_run):
                self.run_a_round_of_inference()

            for i in range(self.N_run):
                elapsed_time, outputs = self.run_a_round_of_inference()
                max_values = np.argmax(outputs,axis=1)

                if i == self.N_run - 1:
                    # 使用 np.take 从 class_labels 中选择标签
                    print(np.take(self.class_labels, max_values[:8]))

                elapsed_time = np.append(elapsed_time, elapsed_time)

                if i % 50 == 0:
                    print('Steps {} - {} average: {:4.1f} ms'.format(i, i+50, (elapsed_time[-50:].mean())*1000))
            print('Throughput: {:.0f} images/s'.format(self.N_run* 32/elapsed_time.sum()))




def main():
    # FP32  1120 images/s
    engine_file_path = '/mnt/c/code/cpp_practice/tensorrt_practice/TensorRT_Tutorial_COLAB/inceptionv3.engine'
    
    # FP16  2182 images/s
    # engine_file_path = '/mnt/c/code/cpp_practice/tensorrt_practice/TensorRT_Tutorial_COLAB/inceptionv3_fp16.engine'
    
    # INT8
    # Polygraph generated engine
    # engine_file_path = '/mnt/c/code/cpp_practice/tensorrt_practice/TensorRT_Tutorial_COLAB/inceptionv3_int8.engine'
    # Int8 generated engine
    # inceptionv3_int8_trtexec.engine
    engine_file_path = '/mnt/c/code/cpp_practice/tensorrt_practice/TensorRT_Tutorial_COLAB/inceptionv3_int8_trtexec.engine'

    path_to_classes = '/mnt/c/code/cpp_practice/tensorrt_practice/TensorRT_Tutorial_COLAB/imagenet-classes.txt'
    # call class instance
    incept_engine = InceptionV3TFEngine(engine_file_path, path_to_classes)        
    incept_engine.inference_run()

if __name__ == '__main__':
    main()