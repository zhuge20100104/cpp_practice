import numpy as np
from tensorflow.keras.preprocessing import image
from tensorflow.keras.applications.inception_v3 import preprocess_input

def load_data():
    batch_size = 32
    batched_input = np.zeros((batch_size, 299, 299, 3), dtype=np.float32)

    for i in range(batch_size):

        img_path = './data/img%d.JPG' % (i % 4)

        img = image.load_img(img_path, target_size=(299, 299))
        x = image.img_to_array(img)
        x = np.expand_dims(x, axis=0)
        x = preprocess_input(x)
        batched_input[i, :] = x
    yield {"keras_tensor": batched_input}