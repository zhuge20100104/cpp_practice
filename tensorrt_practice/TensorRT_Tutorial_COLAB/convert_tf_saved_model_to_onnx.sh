pip install tf2onnx
python -m tf2onnx.convert --saved-model ./inceptionv3_saved_model --output inceptionv3.onnx

# 环境
# Nvidia RTX 3050 Ada
# 32 images per batch
# Tensorflow engine NHWC (32, 299, 299, 3)
# Warmup run: 10
# Inference run: 1000

# Tensorflow 原生
580 images/s

# FP32
522799 images/s
trtexec --onnx=/mnt/c/code/cpp_practice/tensorrt_practice/TensorRT_Tutorial_COLAB/inceptionv3.onnx --saveEngine=/mnt/c/code/cpp_practice/tensorrt_practice/TensorRT_Tutorial_COLAB/inceptionv3.engine --memPoolSize=workspace:6GB --minShapes=keras_tensor:1x299x299x3 --optShapes=keras_tensor:32x299x299x3 --maxShapes=keras_tensor:64x299x299x3 --shapes=keras_tensor:32x299x299x3

# FP16
1124023 images/s
trtexec --onnx=/mnt/c/code/cpp_practice/tensorrt_practice/TensorRT_Tutorial_COLAB/inceptionv3.onnx --saveEngine=/mnt/c/code/cpp_practice/tensorrt_practice/TensorRT_Tutorial_COLAB/inceptionv3_fp16.engine --memPoolSize=workspace:6GB --minShapes=keras_tensor:1x299x299x3 --optShapes=keras_tensor:32x299x299x3 --maxShapes=keras_tensor:64x299x299x3 --shapes=keras_tensor:32x299x299x3 --fp16

# INT8
1655423 images/s

# polygraphy 链接: https://github.com/NVIDIA/TensorRT/blob/main/tools/Polygraphy/examples/cli/convert/01_int8_calibration_in_tensorrt/README.md
# 安装polygraphy
python -m pip install colored polygraphy --extra-index-url https://pypi.ngc.nvidia.com

polygraphy convert inceptionv3.onnx --int8 \
    --data-loader-script ./data_loader.py \
    --calibration-cache identity_calib.cache \
    -o inceptionv3_int8.engine --input-shapes keras_tensor:[32,299,299,3]


trtexec --onnx=/mnt/c/code/cpp_practice/tensorrt_practice/TensorRT_Tutorial_COLAB/inceptionv3.onnx --saveEngine=/mnt/c/code/cpp_practice/tensorrt_practice/TensorRT_Tutorial_COLAB/inceptionv3_int8_trtexec.engine    --int8 \
    --memPoolSize=workspace:6GB --minShapes=keras_tensor:1x299x299x3 --optShapes=keras_tensor:32x299x299x3 --maxShapes=keras_tensor:64x299x299x3 --shapes=keras_tensor:32x299x299x3 \
    --calib=/mnt/c/code/cpp_practice/tensorrt_practice/TensorRT_Tutorial_COLAB/identity_calib.cache