{
 "cells": [
  {
   "cell_type": "markdown",
   "id": "a1abe75f-dfed-43c4-8858-c05966835121",
   "metadata": {},
   "source": [
    "# Intro\n",
    "\n",
    "[PyTorch](https://pytorch.org/) is a very powerful machine learning framework. Central to PyTorch are [tensors](https://pytorch.org/docs/stable/tensors.html), a generalization of matrices to higher ranks. One intuitive example of a tensor is an image with three color channels: A 3-channel (red, green, blue) image which is 64 pixels wide and 64 pixels tall is a $3\\times64\\times64$ tensor. You can access the PyTorch framework by writing `import torch` near the top of your code, along with all of your other import statements."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 1,
   "id": "dce198ee-558e-469e-8726-df38a3350876",
   "metadata": {},
   "outputs": [],
   "source": [
    "import torch"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "ac943172-a025-4cb3-a7fe-5196294dc9ec",
   "metadata": {},
   "source": [
    "# Tensor Properties\n",
    "One way to create tensors from a list or an array is to use `torch.Tensor`. It'll be used to set up examples in this notebook, but you'll never need to use it in the course - in fact, if you find yourself needing it, that's probably not the correct answer. "
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 2,
   "id": "d6d0c979-cd61-4abf-9496-a27542008652",
   "metadata": {},
   "outputs": [],
   "source": [
    "example_tensor = torch.tensor([\n",
    "    [[1, 2], [3, 4]],\n",
    "    [[5, 6], [7, 8]],\n",
    "    [[9, 0], [1, 2]]\n",
    "])"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 3,
   "id": "be3c1791-1fea-47d3-90d9-9b368e559f79",
   "metadata": {},
   "outputs": [
    {
     "data": {
      "text/plain": [
       "torch.Size([3, 2, 2])"
      ]
     },
     "execution_count": 3,
     "metadata": {},
     "output_type": "execute_result"
    }
   ],
   "source": [
    "example_tensor.shape"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 4,
   "id": "8a436cfd-a062-4e76-ad35-e9ec049cc834",
   "metadata": {},
   "outputs": [
    {
     "data": {
      "text/plain": [
       "tensor([[[1, 2],\n",
       "         [3, 4]],\n",
       "\n",
       "        [[5, 6],\n",
       "         [7, 8]],\n",
       "\n",
       "        [[9, 0],\n",
       "         [1, 2]]])"
      ]
     },
     "execution_count": 4,
     "metadata": {},
     "output_type": "execute_result"
    }
   ],
   "source": [
    "example_tensor"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "1a566b0f-1612-4684-a18a-6dd76cb57b08",
   "metadata": {},
   "source": [
    "## Tensor Properties: Device\n",
    "\n",
    "One important property is the device of the tensor - throughout this notebook you'll be sticking to tensors which are on the CPU. However, throughout the course you'll also be using tensors on GPU (that is, a graphics card which will be provided for you to use for the course). To view the device of the tensor, all you need to write is `example_tensor.device`. To move a tensor to a new device, you can write `new_tensor = example_tensor.to(device)` where device will be either `cpu` or `cuda`."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 5,
   "id": "f1955517-6367-4a62-91c3-971bdc2c50a3",
   "metadata": {},
   "outputs": [
    {
     "data": {
      "text/plain": [
       "device(type='cpu')"
      ]
     },
     "execution_count": 5,
     "metadata": {},
     "output_type": "execute_result"
    }
   ],
   "source": [
    "example_tensor.device"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "c4e8a518-40b7-45b9-85be-9bf5b2383340",
   "metadata": {},
   "source": [
    "## Tensor Properties: Shape\n",
    "\n",
    "And you can get the number of elements in each dimension by printing out the tensor's shape, using `example_tensor.shape`, something you're likely familiar with if you've used numpy. For example, this tensor is a $3\\times2\\times2$ tensor, since it has 3 elements, each of which are $2\\times2$. "
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 6,
   "id": "9d9755b0-bfa2-4b17-a432-9bdc647c671e",
   "metadata": {},
   "outputs": [
    {
     "data": {
      "text/plain": [
       "torch.Size([3, 2, 2])"
      ]
     },
     "execution_count": 6,
     "metadata": {},
     "output_type": "execute_result"
    }
   ],
   "source": [
    "example_tensor.shape"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "92af4579-0df7-464f-a953-88ae56d3448c",
   "metadata": {},
   "source": [
    "You can also get the size of a particular dimension $n$ using `example_tensor.shape[n]` or equivalently `example_tensor.size(n)`"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 7,
   "id": "02815912-336b-4989-b430-2e4fdf74fbcb",
   "metadata": {},
   "outputs": [
    {
     "name": "stdout",
     "output_type": "stream",
     "text": [
      "shape[0] =  3\n",
      "shape[1] =  2\n"
     ]
    }
   ],
   "source": [
    "print('shape[0] = ', example_tensor.shape[0])\n",
    "print('shape[1] = ', example_tensor.size(1))"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "6cbdcd0f-b4ec-4119-9946-2e6940463dfa",
   "metadata": {},
   "source": [
    "Finally, it is sometimes useful to get the number of dimensions (rank) or the number of elements, which you can do as follows"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 9,
   "id": "50caf6ae-9ff5-47b0-990c-d9096a8e201a",
   "metadata": {},
   "outputs": [
    {
     "name": "stdout",
     "output_type": "stream",
     "text": [
      "Rank =  3\n",
      "Number of elements =  12\n"
     ]
    }
   ],
   "source": [
    "print('Rank = ', len(example_tensor.shape))\n",
    "print('Number of elements = ', example_tensor.numel())"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "ebb4259d-5fdf-4a5c-8150-c198ea3c97f9",
   "metadata": {},
   "source": [
    " # Indexing Tensors\n",
    "\n",
    "As with numpy, you can access specific elements or subsets of elements of a tensor. To access the $n$-th element, you can simply write `example_tensor[n]` - as with Python in general, these dimensions are 0-indexed. "
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 10,
   "id": "35c5c33b-a31a-41e6-90b9-f6b3f15d8214",
   "metadata": {},
   "outputs": [
    {
     "data": {
      "text/plain": [
       "tensor([[[1, 2],\n",
       "         [3, 4]],\n",
       "\n",
       "        [[5, 6],\n",
       "         [7, 8]],\n",
       "\n",
       "        [[9, 0],\n",
       "         [1, 2]]])"
      ]
     },
     "execution_count": 10,
     "metadata": {},
     "output_type": "execute_result"
    }
   ],
   "source": [
    "example_tensor"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 11,
   "id": "1ae40f4b-0bcf-4159-9d35-9823a82c6afe",
   "metadata": {},
   "outputs": [
    {
     "data": {
      "text/plain": [
       "tensor([[3, 4],\n",
       "        [7, 8],\n",
       "        [1, 2]])"
      ]
     },
     "execution_count": 11,
     "metadata": {},
     "output_type": "execute_result"
    }
   ],
   "source": [
    "example_tensor[:, 1]"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 12,
   "id": "8ac338ea-4fe2-442e-ad3b-9b04f2cb75d4",
   "metadata": {},
   "outputs": [
    {
     "data": {
      "text/plain": [
       "torch.Size([3, 2])"
      ]
     },
     "execution_count": 12,
     "metadata": {},
     "output_type": "execute_result"
    }
   ],
   "source": [
    "example_tensor[:, 1].shape"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 14,
   "id": "f85490e6-38c7-4d64-a2f8-3df291775521",
   "metadata": {},
   "outputs": [
    {
     "data": {
      "text/plain": [
       "tensor([[5, 6],\n",
       "        [7, 8]])"
      ]
     },
     "execution_count": 14,
     "metadata": {},
     "output_type": "execute_result"
    }
   ],
   "source": [
    "example_tensor[1]"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "e94ba52c-7e97-4afc-9d0f-8dea570855e1",
   "metadata": {},
   "source": [
    "In addition, if you want to access the $j$-th dimension of the $i$-th example, you can write `example_tensor[i, j]`"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 15,
   "id": "a932f5b0-9778-4356-87e7-a9a40ccefee9",
   "metadata": {},
   "outputs": [
    {
     "data": {
      "text/plain": [
       "tensor(7)"
      ]
     },
     "execution_count": 15,
     "metadata": {},
     "output_type": "execute_result"
    }
   ],
   "source": [
    "example_tensor[1, 1, 0]"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "e02b3e03-15b5-42cd-b9c2-34c16e00ad85",
   "metadata": {},
   "source": [
    "Note that if you'd like to get a Python scalar value from a tensor, you can use `example_scalar.item()`"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 16,
   "id": "274b0d89-c159-4a29-a0e3-32d6b1dd893e",
   "metadata": {},
   "outputs": [
    {
     "data": {
      "text/plain": [
       "7"
      ]
     },
     "execution_count": 16,
     "metadata": {},
     "output_type": "execute_result"
    }
   ],
   "source": [
    "example_scalar = example_tensor[1, 1, 0]\n",
    "example_scalar.item()"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 19,
   "id": "cd65a6a7-ccd9-4c80-98c7-e5cbea867581",
   "metadata": {},
   "outputs": [
    {
     "data": {
      "text/plain": [
       "torch.Tensor"
      ]
     },
     "execution_count": 19,
     "metadata": {},
     "output_type": "execute_result"
    }
   ],
   "source": [
    "type(example_tensor[1, 1, 0])"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "8c0bc7df-9820-4a15-9d9a-51835411cc14",
   "metadata": {},
   "source": [
    "In addition, you can index into the ith element of a column by using `x[:, i]`. For example, if you want the top-left element of each element in `example_tensor`, which is the `0, 0` element of each matrix, you can write:"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 20,
   "id": "4bbc1a68-c017-4507-80c1-645d22896b8a",
   "metadata": {},
   "outputs": [
    {
     "data": {
      "text/plain": [
       "tensor([1, 5, 9])"
      ]
     },
     "execution_count": 20,
     "metadata": {},
     "output_type": "execute_result"
    }
   ],
   "source": [
    "example_tensor[:, 0, 0]"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "499690a2-f01a-4add-be0f-15b7955aa066",
   "metadata": {},
   "source": [
    "# Initializing Tensors\n",
    "\n",
    "There are many ways to create new tensors in PyTorch, but in this course, the most important ones are: \n",
    "\n",
    "[`torch.ones_like`](https://pytorch.org/docs/master/generated/torch.ones_like.html): creates a tensor of all ones with the same shape and device as `example_tensor`."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 22,
   "id": "7a995932-7911-4c22-9355-2a73d94b87d1",
   "metadata": {},
   "outputs": [
    {
     "data": {
      "text/plain": [
       "tensor([[[1, 1],\n",
       "         [1, 1]],\n",
       "\n",
       "        [[1, 1],\n",
       "         [1, 1]],\n",
       "\n",
       "        [[1, 1],\n",
       "         [1, 1]]])"
      ]
     },
     "execution_count": 22,
     "metadata": {},
     "output_type": "execute_result"
    }
   ],
   "source": [
    "torch.ones_like(example_tensor)"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "88612d38-f047-49f9-9dbc-499016cabbb2",
   "metadata": {},
   "source": [
    "[`torch.zeros_like`](https://pytorch.org/docs/master/generated/torch.zeros_like.html): creates a tensor of all zeros with the same shape and device as `example_tensor`"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 24,
   "id": "05eb304f-46c0-4b4d-a1d9-1efa7c629a10",
   "metadata": {},
   "outputs": [
    {
     "data": {
      "text/plain": [
       "tensor([[[0, 0],\n",
       "         [0, 0]],\n",
       "\n",
       "        [[0, 0],\n",
       "         [0, 0]],\n",
       "\n",
       "        [[0, 0],\n",
       "         [0, 0]]])"
      ]
     },
     "execution_count": 24,
     "metadata": {},
     "output_type": "execute_result"
    }
   ],
   "source": [
    "torch.zeros_like(example_tensor)"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "83c4114b-f695-4f28-a94a-7d39b6455b0d",
   "metadata": {},
   "source": [
    "[`torch.randn_like`](https://pytorch.org/docs/stable/generated/torch.randn_like.html): creates a tensor with every element sampled from a [Normal (or Gaussian) distribution](https://en.wikipedia.org/wiki/Normal_distribution) with the same shape and device as `example_tensor`\n"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 27,
   "id": "6b3f46d1-7fb0-4868-acd5-d1b2b4116ac1",
   "metadata": {},
   "outputs": [
    {
     "data": {
      "text/plain": [
       "tensor([[[ 0.2190, -0.3185],\n",
       "         [ 1.0463,  0.2293]],\n",
       "\n",
       "        [[ 1.7862,  0.4135],\n",
       "         [ 1.3815, -0.1729]],\n",
       "\n",
       "        [[-1.8598,  1.5978],\n",
       "         [ 0.7444,  0.1579]]])"
      ]
     },
     "execution_count": 27,
     "metadata": {},
     "output_type": "execute_result"
    }
   ],
   "source": [
    "example_tensor = example_tensor.to(torch.float32)\n",
    "torch.randn_like(example_tensor)"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 28,
   "id": "85590c11-e0f0-401c-af36-1bfbccf2a120",
   "metadata": {},
   "outputs": [
    {
     "data": {
      "text/plain": [
       "tensor([[-0.0861, -0.6409, -0.3455],\n",
       "        [ 0.4596,  0.2157, -2.5457],\n",
       "        [-1.6004,  1.8016, -0.4793]], device='cuda:0')"
      ]
     },
     "execution_count": 28,
     "metadata": {},
     "output_type": "execute_result"
    }
   ],
   "source": [
    "torch.randn(3, 3, device='cuda')"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 29,
   "id": "1bacb91b-77c9-490c-b9ee-1d4c3be81ed0",
   "metadata": {},
   "outputs": [
    {
     "data": {
      "text/plain": [
       "tensor([[-0.3126,  0.1909],\n",
       "        [-1.3165,  1.1254]])"
      ]
     },
     "execution_count": 29,
     "metadata": {},
     "output_type": "execute_result"
    }
   ],
   "source": [
    "torch.randn(2, 2, device='cpu')"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "bfcd3d18-ab7f-4927-84fc-b63b1a24a0c6",
   "metadata": {},
   "source": [
    "# Basic Functions\n",
    "\n",
    "There are a number of basic functions that you should know to use PyTorch - if you're familiar with numpy, all commonly-used functions exist in PyTorch, usually with the same name. You can perform element-wise multiplication / division by a scalar $c$ by simply writing `c * example_tensor`, and element-wise addition / subtraction by a scalar by writing `example_tensor + c`\n",
    "\n",
    "Note that most operations are not in-place in PyTorch, which means that they don't change the original variable's data (However, you can reassign the same variable name to the changed data if you'd like, such as `example_tensor = example_tensor + 1`)"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 31,
   "id": "42f310a0-e8e2-4675-9bc8-9ecacd664ffb",
   "metadata": {},
   "outputs": [
    {
     "data": {
      "text/plain": [
       "tensor([[[1., 2.],\n",
       "         [3., 4.]],\n",
       "\n",
       "        [[5., 6.],\n",
       "         [7., 8.]],\n",
       "\n",
       "        [[9., 0.],\n",
       "         [1., 2.]]])"
      ]
     },
     "execution_count": 31,
     "metadata": {},
     "output_type": "execute_result"
    }
   ],
   "source": [
    "example_tensor"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 33,
   "id": "d5847131-6daa-44fc-b438-c626cbcd8014",
   "metadata": {},
   "outputs": [
    {
     "data": {
      "text/plain": [
       "tensor([[[ -8.,  -6.],\n",
       "         [ -4.,  -2.]],\n",
       "\n",
       "        [[  0.,   2.],\n",
       "         [  4.,   6.]],\n",
       "\n",
       "        [[  8., -10.],\n",
       "         [ -8.,  -6.]]])"
      ]
     },
     "execution_count": 33,
     "metadata": {},
     "output_type": "execute_result"
    }
   ],
   "source": [
    "(example_tensor - 5) * 2"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "ac537cf4-de87-4398-a964-7447960cfa40",
   "metadata": {},
   "source": [
    "You can calculate the mean or standard deviation of a tensor using [`example_tensor.mean()`](https://pytorch.org/docs/stable/generated/torch.mean.html) or [`example_tensor.std()`](https://pytorch.org/docs/stable/generated/torch.std.html). "
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 35,
   "id": "70f14374-348b-4ad6-8e08-8f8f9c8ef1ee",
   "metadata": {},
   "outputs": [
    {
     "name": "stdout",
     "output_type": "stream",
     "text": [
      "Mean:  tensor(4.)\n",
      "stddev:  tensor(2.9848)\n"
     ]
    }
   ],
   "source": [
    "print('Mean: ', example_tensor.mean())\n",
    "print('stddev: ', example_tensor.std())"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "68828419-3d71-442e-a7b1-2974f2963a98",
   "metadata": {},
   "source": [
    "You might also want to find the mean or standard deviation along a particular dimension. To do this you can simple pass the number corresponding to that dimension to the function. For example, if you want to get the average $2\\times2$ matrix of the $3\\times2\\times2$ `example_tensor` you can write:"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "afb30ed2-1d15-4b70-9d86-5a25c05fc234",
   "metadata": {},
   "source": [
    "在你的例子中，`example_tensor.mean(0)` 是对 `example_tensor` 在第一个维度（即维度 0）上进行求均值操作。\n",
    "\n",
    "假设 `example_tensor` 的形状是 `(3, 2, 2)`，即它包含 3 个 2x2 的矩阵。我们来逐步解释这个操作。\n",
    "\n",
    "### `example_tensor` 的形状和内容：\n",
    "```python\n",
    "tensor([[[1., 2.],\n",
    "         [3., 4.]],\n",
    "\n",
    "        [[5., 6.],\n",
    "         [7., 8.]],\n",
    "\n",
    "        [[9., 0.],\n",
    "         [1., 2.]]])\n",
    "```\n",
    "这是一个形状为 `(3, 2, 2)` 的张量，表示 3 个 2x2 的矩阵。\n",
    "\n",
    "### 求均值操作：\n",
    "`mean(0)` 会在第一个维度（即批次维度，3 个 2x2 的矩阵）上进行求均值操作。也就是说，`mean(0)` 会在维度 0 上对每个位置的元素求平均值。\n",
    "\n",
    "### 逐元素计算均值：\n",
    "我们可以分步计算每个位置的均值。\n",
    "\n",
    "1. **第一行第一列：**\n",
    "   - `1 + 5 + 9 = 15`\n",
    "   - 均值：`15 / 3 = 5.0000`\n",
    "\n",
    "2. **第一行第二列：**\n",
    "   - `2 + 6 + 0 = 8`\n",
    "   - 均值：`8 / 3 = 2.6667`\n",
    "\n",
    "3. **第二行第一列：**\n",
    "   - `3 + 7 + 1 = 11`\n",
    "   - 均值：`11 / 3 = 3.6667`\n",
    "\n",
    "4. **第二行第二列：**\n",
    "   - `4 + 8 + 2 = 14`\n",
    "   - 均值：`14 / 3 = 4.6667`\n",
    "\n",
    "### 结果：\n",
    "`example_tensor.mean(0)` 计算出的均值结果是：\n",
    "```python\n",
    "tensor([[5.0000, 2.6667],\n",
    "        [3.6667, 4.6667]])\n",
    "```\n",
    "\n",
    "这就是如何在维度 0 上对每个位置的元素进行求均值操作的过程。"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 37,
   "id": "a711736f-67b2-45fe-9bcc-04f02179061c",
   "metadata": {},
   "outputs": [
    {
     "data": {
      "text/plain": [
       "tensor([[5.0000, 2.6667],\n",
       "        [3.6667, 4.6667]])"
      ]
     },
     "execution_count": 37,
     "metadata": {},
     "output_type": "execute_result"
    }
   ],
   "source": [
    "example_tensor.mean(0)"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 38,
   "id": "c0b4e7e9-92f7-4a0f-ab28-372e02857dd3",
   "metadata": {},
   "outputs": [
    {
     "data": {
      "text/plain": [
       "tensor([[5.0000, 2.6667],\n",
       "        [3.6667, 4.6667]])"
      ]
     },
     "execution_count": 38,
     "metadata": {},
     "output_type": "execute_result"
    }
   ],
   "source": [
    "example_tensor.mean(axis=0)"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 39,
   "id": "c4b93b53-911d-4489-b9aa-e699c1f20216",
   "metadata": {},
   "outputs": [
    {
     "data": {
      "text/plain": [
       "tensor([[5.0000, 2.6667],\n",
       "        [3.6667, 4.6667]])"
      ]
     },
     "execution_count": 39,
     "metadata": {},
     "output_type": "execute_result"
    }
   ],
   "source": [
    "example_tensor.mean(dim=0)"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "2449cf50-d15c-4f45-83a2-d0a664f32424",
   "metadata": {},
   "source": [
    "# PyTorch Neural Network Module (`torch.nn`)\n",
    "\n",
    "PyTorch has a lot of powerful classes in its `torch.nn` module (Usually, imported as simply `nn`). These classes allow you to create a new function which transforms a tensor in specific way, often retaining information when called multiple times.m"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "0a3b2873-51d9-4b67-97ed-7dab6542aa33",
   "metadata": {},
   "outputs": [],
   "source": []
  }
 ],
 "metadata": {
  "kernelspec": {
   "display_name": "Python 3 (ipykernel)",
   "language": "python",
   "name": "python3"
  },
  "language_info": {
   "codemirror_mode": {
    "name": "ipython",
    "version": 3
   },
   "file_extension": ".py",
   "mimetype": "text/x-python",
   "name": "python",
   "nbconvert_exporter": "python",
   "pygments_lexer": "ipython3",
   "version": "3.10.12"
  }
 },
 "nbformat": 4,
 "nbformat_minor": 5
}
