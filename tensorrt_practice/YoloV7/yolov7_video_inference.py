'''
    Python packages
    os
    cv2
    time
'''

import os
import cv2
import time

'''
    class components
    class name: YoloV7DNN
    class init components: nms_threshold, conf_threshold, class_labels, video_file, yolov4: [path_to_cfg_yolo, path_to_weights]
    target: YoloV7 DNN Inference on Image 
'''


# TODO: Refact it to video inference
class YoloV7DNN_Detection:

    '''Init of paramaters'''
    def __init__(self, nms_threshold, conf_threshold, class_labels, video_file, path_to_cfg, path_to_weights):
        
        self.nms_threshold = nms_threshold
        self.conf_threshold = conf_threshold
        self.class_labels = class_labels
        self.video_file = video_file
        self.path_to_cfg = path_to_cfg
        self.path_to_weights = path_to_weights

        with open(class_labels, 'r') as read_class:
            self.class_labels = [classes.strip() for classes in read_class.readlines()]
    
    '''
        target: Inference DNN OpenCV with ONNX
        param[1]: path to yolov4 configuration
        param[2]: path to yolov4 weights
    '''
    def inference_dnn(self, frame, path_to_cfg, path_to_weights):
        network = cv2.dnn.readNetFromDarknet(path_to_cfg, path_to_weights)

        # gpu or cpu
        network.setPreferableBackend(cv2.dnn.DNN_BACKEND_CUDA)
        network.setPreferableTarget(cv2.dnn.DNN_TARGET_CUDA_FP16)

        #creates net from file with trained weights and config, 
        model = cv2.dnn.DetectionModel(network)

        # set model parameters
        model.setInputParams(size=(640, 640), scale=1/255, swapRB=True)

        classes, scores, boxes = model.detect(frame, self.conf_threshold, self.nms_threshold)

        return classes, scores, boxes

    '''
        target: Inference run and draw bounding boxes
        param[1]: image
    '''
    def inference_run(self):
        # video capture is reading
        video_cap = cv2.VideoCapture(self.video_file)

        if not video_cap.isOpened():
            print('Video file read unsucessfully!')
            return 
        
        while video_cap.isOpened(): 
            grapped, frame = video_cap.read()

            if not grapped:
                exit()

            frame = cv2.resize(frame, (1000, 800))

            start = time.time()
            getClasses, getScores, getBoxes = self.inference_dnn(frame, self.path_to_cfg, self.path_to_weights)
            end = time.time()

            # frame time 
            frame_time = (end - start) * 1000
            # frame per second 
            FPS = 1/ (end - start)
    
            for (class_id, score, box) in zip(getClasses, getScores, getBoxes):
                print(f'Box coordinates: ', box)
                
                cv2.rectangle(frame, box, (114, 114, 114), 2)
                label = "Frame Time : %.2f ms, FPS : %.2f , ID: %s, Score: %.2f," % (frame_time, FPS ,self.class_labels[class_id], score)
                cv2.putText(frame, label, (box[0] + 30, box[1] + 10), cv2.FONT_HERSHEY_COMPLEX, 0.5, (0, 0, 255), 2)
            
        
            # show image
            cv2.imshow('Image Detected', frame)
            
            if cv2.waitKey(1) & 0xFF == ord('q'):
                break
        
        video_cap.release()
        cv2.destroyAllWindows()


def main():
    path_to_classes = '/mnt/c/code/cpp_practice/tensorrt_practice/YoloV7/coco-classes.txt'
    video_file = '/mnt/c/code/cpp_practice/tensorrt_practice/YoloV7/cars.mp4'
    path_to_cfg_yolov7 = '/mnt/c/code/cpp_practice/tensorrt_practice/YoloV7/yolov7-tiny.cfg'
    path_to_weights_yolov7 = '/mnt/c/code/cpp_practice/tensorrt_practice/YoloV7/yolov7-tiny.weights'

    ## call class instance

    yolov7_video_detection = YoloV7DNN_Detection(0.3, 0.38, path_to_classes, video_file, path_to_cfg_yolov7, path_to_weights_yolov7)
    yolov7_video_detection.inference_run()


if __name__ == '__main__':
    main()