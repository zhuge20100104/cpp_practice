import torch
import numpy as np

# 加载YOLOv7-tiny模型
# Load fine-tuned custom model
model = torch.hub.load('WongKinYiu/yolov7', 'custom', './yolov7.pt',
                      trust_repo=True)

device = torch.device('cuda')
model.model.to(device)

image = torch.rand(1, 3, 640, 640).to(device)

model.model.eval()

onnx_file_path = './yolov7.onnx'



torch.onnx.export(
    model.model,
    image,
    onnx_file_path,
    export_params=True, # 保存权重
    opset_version=12,
    input_names=['images'],
    output_names=['output'],
    keep_initializers_as_inputs=False,
    dynamic_axes={'images': {0: 'batch'}, 'output': {0: 'batch'}} # 动态batch维度
)

print(f"ONNX 模型已成功导出到 {onnx_file_path}")