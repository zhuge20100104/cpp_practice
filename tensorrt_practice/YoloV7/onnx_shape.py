import onnx

# 加载 ONNX 模型
model_path = "./yolov7-tiny.onnx"  # 替换为你的 ONNX 模型路径
model = onnx.load(model_path)

# 打印模型的输入和输出信息
print("== Inputs ==")
for input_tensor in model.graph.input:
    name = input_tensor.name
    shape = [dim.dim_value if dim.dim_value > 0 else "dynamic" for dim in input_tensor.type.tensor_type.shape.dim]
    print(f"Name: {name}, Shape: {shape}")

print("\n== Outputs ==")
for output_tensor in model.graph.output:
    name = output_tensor.name
    shape = [dim.dim_value if dim.dim_value > 0 else "dynamic" for dim in output_tensor.type.tensor_type.shape.dim]
    print(f"Name: {name}, Shape: {shape}")
