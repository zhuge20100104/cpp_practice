import onnx

# 加载模型
model = onnx.load("yolov7.onnx")

# 查看模型的输出节点
print("Before:", [output.name for output in model.graph.output])

# 创建一个新的输出列表，仅保留需要的输出
needed_outputs = ["output"]  # 需要保留的输出节点名称
new_outputs = [o for o in model.graph.output if o.name in needed_outputs]

# 清空现有的输出列表，然后重新添加需要的输出
model.graph.ClearField("output")
model.graph.output.extend(new_outputs)


# 保存修改后的模型
onnx.save(model, "yolov7-out.onnx")

print("After:", [output.name for output in model.graph.output])
