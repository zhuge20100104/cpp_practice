'''
    Python packages
    os
    cv2
    time
'''

import os
import cv2
import time

'''
    class components
    class name: YoloV7DNN
    class init components: nms_threshold, conf_threshold, class_labels, image_path, yolov4: [path_to_cfg_yolo, path_to_weights]
    target: YoloV7 DNN Inference on Image 
'''


class YoloV7DNN_Detection:

    '''Init of paramaters'''
    def __init__(self, nms_threshold, conf_threshold, class_labels, image_path, path_to_cfg, path_to_weights):
        
        self.nms_threshold = nms_threshold
        self.conf_threshold = conf_threshold
        self.class_labels = class_labels
        self.image_path = image_path
        self.path_to_cfg = path_to_cfg
        self.path_to_weights = path_to_weights

        with open(class_labels, 'r') as read_class:
            self.class_labels = [classes.strip() for classes in read_class.readlines()]

        # frame image 
        # load images
        self.frames = self.load_images(self.image_path)

        # preprocess images and resize it
        for self.frame in self.frames:
            self.image = cv2.imread(self.frame)

            # get height and width of images
            self.original_h, self.original_w = self.image.shape[:2]
            
            dimension = (640, 640)

            # resize images 
            self.resize_image = cv2.resize(self.image, dimension, interpolation=cv2.INTER_AREA)

            # get new height and width of the resized image
            self.new_h, self.new_w = self.resize_image.shape[:2]

            # call function inference run
            self.inference_run(self.resize_image)
    
    '''
        Function target: Load images
        param[1]: self
        param[2]: image_path
    '''
    def load_images(self, image_path):
        # list of images
        img_list = []

        for img_original in os.listdir(image_path):
            if img_original.endswith('.jpg') or img_original.endswith('.jpeg') or img_original.endswith('.png'):
                img_full_path = os.path.join(image_path, img_original)
                img_list.append(img_full_path)
        return img_list

    '''
        target: Inference DNN OpenCV with ONNX
        param[1]: path to yolov4 configuration
        param[2]: path to yolov4 weights
    '''
    def inference_dnn(self, path_to_cfg, path_to_weights):
        network = cv2.dnn.readNetFromDarknet(path_to_cfg, path_to_weights)

        # gpu or cpu
        network.setPreferableBackend(cv2.dnn.DNN_BACKEND_CUDA)
        network.setPreferableTarget(cv2.dnn.DNN_TARGET_CUDA_FP16)

        #creates net from file with trained weights and config, 
        model = cv2.dnn.DetectionModel(network)

        # set model parameters
        model.setInputParams(size=(640, 640), scale=1/255, swapRB=True)

        classes, scores, boxes = model.detect(self.image, self.conf_threshold, self.nms_threshold)

        return classes, scores, boxes

    '''
        target: Inference run and draw bounding boxes
        param[1]: image
    '''
    def inference_run(self, image):
        start = time.time()
        getClasses, getScores, getBoxes = self.inference_dnn(self.path_to_cfg, self.path_to_weights)
        end = time.time()

        # frame time 
        frame_time = (end - start) * 1000
        # frame per second 
        FPS = 1/ (end - start)

        '''
        calculate new scale of image which is image formed between original and resized
        '''
        ratio_h = self.new_h / self.original_h
        ratio_w = self.new_w / self.original_w

        for (class_id, score, box) in zip(getClasses, getScores, getBoxes):
            print(f'Box coordinates: ', box)
            # normalize bounding box to detection
            box[0] = int(box[0] * ratio_w) # x
            box[1] = int(box[1] * ratio_h) # y
            box[2] = int(box[2] * ratio_w) # w
            box[3] = int(box[3] * ratio_h) # h

            cv2.rectangle(image, box, (114, 114, 114), 2)
            label = "Frame Time : %.2f ms, FPS : %.2f , ID: %s, Score: %.2f," % (frame_time, FPS ,self.class_labels[class_id], score)
            cv2.putText(image, label, (box[0] + 30, box[1] + 10), cv2.FONT_HERSHEY_COMPLEX, 0.5, (0, 0, 255), 2)
        
    
        # show image
        cv2.imshow('Image Detected', image)
        
        cv2.waitKey(4000)
        cv2.destroyAllWindows()



def main():
    path_to_classes = '/mnt/c/code/cpp_practice/tensorrt_practice/YoloV7/coco-classes.txt'
    image_path = '/mnt/c/code/cpp_practice/tensorrt_practice/YoloV7/Images'
    path_to_cfg_yolov7 = '/mnt/c/code/cpp_practice/tensorrt_practice/YoloV7/yolov7-tiny.cfg'
    path_to_weights_yolov7 = '/mnt/c/code/cpp_practice/tensorrt_practice/YoloV7/yolov7-tiny.weights'

    ## call class instance

    YoloV7DNN_Detection(0.3, 0.38, path_to_classes, image_path, path_to_cfg_yolov7, path_to_weights_yolov7)


if __name__ == '__main__':
    main()