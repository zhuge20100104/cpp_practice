trtexec --onnx=/mnt/c/code/cpp_practice/tensorrt_practice/YoloV7/yolov7-tiny-out.onnx \
 --saveEngine=/mnt/c/code/cpp_practice/tensorrt_practice/YoloV7/yolov7-tiny.engine \
 --memPoolSize=workspace:8GB \
 --minShapes=images:1x3x640x640 --optShapes=images:1x3x640x640 --maxShapes=images:1x3x640x640 --shapes=images:1x3x640x640



trtexec --onnx=/mnt/c/code/cpp_practice/tensorrt_practice/YoloV7/yolov7-out.onnx \
 --saveEngine=/mnt/c/code/cpp_practice/tensorrt_practice/YoloV7/yolov7.engine \
 --memPoolSize=workspace:12GB \
 --minShapes=images:1x3x640x640 --optShapes=images:1x3x640x640 --maxShapes=images:1x3x640x640 --shapes=images:1x3x640x640


# 相关步骤

# 1. 从Yolov7 网站下载对应的pt文件，pytorch的model
# 直接看他的Release页面就行
# https://github.com/WongKinYiu/yolov7/releases

# 2. 将yolov7.pt文件转换成onnx格式
# python download_yolov7.py

# 3. yolov7的onnx格式的输出有多余的中间输出，需要输出，仅保留output
# python onnx_remove_output.py

# 4. 使用trtexec 将onnx文件转换成 trt engine
trtexec --onnx=/mnt/c/code/cpp_practice/tensorrt_practice/YoloV7/yolov7-out.onnx \
 --saveEngine=/mnt/c/code/cpp_practice/tensorrt_practice/YoloV7/yolov7.engine \
 --memPoolSize=workspace:12GB \
 --minShapes=images:1x3x640x640 --optShapes=images:1x3x640x640 --maxShapes=images:1x3x640x640 --shapes=images:1x3x640x640

# 4 写trt 推理文件，然后运行
# python yolov7_trt_inference.py