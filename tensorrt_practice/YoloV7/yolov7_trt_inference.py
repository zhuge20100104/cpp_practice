'''
    是的，你的理解是正确的！**YOLO 模型中总共有 9 种不同大小的 anchor（瞄点框）**，每 3 种 anchor 对应一种特定的特征图（图片尺寸分割），以下是更具体的解释：

    ---

    ### **为什么是 9 种大小的瞄点框？**
    YOLO 的 anchor 是用来预测物体位置的参考框。YOLOv7 使用了 3 个特征图，每个特征图的每个像素点都配备了 3 个 anchor，因此总共有 \(3 \times 3 = 9\) 种不同大小的 anchor。

    ---

    ### **分配方式：**
    1. **80×80 特征图**  
    - 负责检测**小目标**。  
    - 每个像素点有 3 个 anchor，总共有 \(80 \times 80 \times 3 = 19,200\) 个 anchor。

    2. **40×40 特征图**  
    - 负责检测**中等目标**。  
    - 每个像素点有 3 个 anchor，总共有 \(40 \times 40 \times 3 = 4,800\) 个 anchor。

    3. **20×20 特征图**  
    - 负责检测**大目标**。  
    - 每个像素点有 3 个 anchor，总共有 \(20 \times 20 \times 3 = 1,200\) 个 anchor。

    ---

    ### **每种分辨率对应的 3 个 anchor 尺寸**
    每个特征图都对应 3 种不同大小的 anchor box。模型的训练时会根据数据集自动生成这些 anchor box 的尺寸（通过 k-means 聚类得到），例如：
    - 80×80 特征图可能对应小尺寸的 anchor box，例如 \((10\times13)\)、\((16\times30)\)、\((33\times23)\)。
    - 40×40 特征图可能对应中等尺寸的 anchor box，例如 \((30\times61)\)、\((62\times45)\)、\((59\times119)\)。
    - 20×20 特征图可能对应大尺寸的 anchor box，例如 \((116\times90)\)、\((156\times198)\)、\((373\times326)\)。

    （这些具体大小是根据你的数据集计算的，只是举例。）

    ---

    ### **为什么有 3 个特征图？**
    这是为了适应不同大小的目标：
    - **80×80 特征图**负责捕捉细节，用来检测小目标（例如人脸、小物体）。
    - **40×40 特征图**负责检测中等目标（例如车辆、动物）。
    - **20×20 特征图**负责检测大目标（例如建筑物、飞机）。

    每个特征图的分辨率不同，代表了不同的感受野（receptive field）。小分辨率的特征图可以检测大目标，因为它的感受野大；高分辨率的特征图则适合检测小目标。

    ---

    ### **总结**
    - 总共有 **9 种 anchor（瞄点框）大小**，分别分布在 3 个特征图上。
    - **每个特征图对应 3 种不同大小的 anchor**，分别适合检测不同尺寸的目标。
    - 这 9 种 anchor 被设计用来覆盖目标的各种大小，确保从小物体到大物体都能被检测到。

    所以，模型的每个输出特征图都利用了这 3 种 anchor 框来预测物体的位置和类别，最终输出的是一个 \(25,200 \times 85\) 的大矩阵，包含了所有的预测结果。
'''


import tensorrt as trt
import pycuda.autoinit
import pycuda.driver as cuda
import cv2
import numpy as np
import os 
from PIL import Image
import matplotlib.pyplot as plt
import yaml
import time 

class TRTInference:

    # specify engine file path and input and output shape
    def __init__(self, engine_file_path, input_shape, output_shape, class_labels_file, conf_threshold, score_threshold, nms_threshold):
        self.logger = trt.Logger(trt.Logger.WARNING)
        self.engine_file_path = engine_file_path

        # load engine here
        self.engine = self.load_engine(self.engine_file_path)

        # create context
        self.context = self.engine.create_execution_context()

        self.conf_threshold = conf_threshold
        self.score_threshold = score_threshold
        self.nms_threshold = nms_threshold

        # input shape 
        self.input_shape = input_shape
        self.class_labels_file = class_labels_file
        self.count = 0

        # output shape
        self.output_shape = output_shape
        with open(class_labels_file, 'r') as class_read:
            data = yaml.safe_load(class_read)
            self.class_labels = [name for name in data['names'].values()]

    def load_engine(self, engine_file_path):
        with open(engine_file_path, 'rb') as f:
            runtime = trt.Runtime(self.logger)
            engine_deserialized = runtime.deserialize_cuda_engine(f.read())
            return engine_deserialized
    
    def preprocess_image(self, image_path):
        img_list = []
        img_path = []

        count = 0

        for img_original in os.listdir(image_path):
            img_full_path = os.path.join(image_path, img_original)
            self.img = cv2.imread(img_full_path)
            self.org_h, self.org_w = self.img[:2]
            self.img_resized = cv2.resize(self.img, (self.input_shape[2], self.input_shape[3]), interpolation=cv2.INTER_AREA)
            img_np = np.array(self.img_resized).astype(np.float32)/ 255.0
            img_np = img_np.transpose((2, 0, 1))
            img_np = np.expand_dims(img_np, axis=0)
            self.resized_imgH, self.resized_imgW = self.img_resized.shape[:2]
            count += 1
            img_list.append(img_np)
            img_path.append(img_full_path)

            # Only select 12 pictures
            if count >= 12:
                continue
        return img_list, img_path

    def inference_detection(self, image_path):
        input_list, full_img_paths = self.preprocess_image(image_path)
        
        self.total_time = 0
        self.num_frames = len(input_list)
        
        # 确保设置输入张量的形状
        self.context.set_input_shape("images", self.input_shape)
        
        for inputs, full_img_path in zip(input_list, full_img_paths):
            # start time
            self.start = time.time()
            inputs = np.ascontiguousarray(inputs)
            outputs = np.empty(self.output_shape, dtype=np.float32)

            d_inputs = cuda.mem_alloc(1 * inputs.nbytes)
            d_outputs = cuda.mem_alloc(1 * outputs.nbytes)

            bindings = [d_inputs, d_outputs]
            
            cuda.memcpy_htod(d_inputs, inputs)

            self.context.execute_v2(bindings)

            cuda.memcpy_dtoh(outputs, d_outputs)

            d_inputs.free()
            d_outputs.free()

            self.end = time.time()

            self.total_time += (self.end - self.start)

            self.fps = self.num_frames / self.total_time
            
            self.postprocess_recognized_image(full_img_path, outputs)
    
    def postprocess_recognized_image(self, image_path, yolov5_output):
        image = cv2.imread(image_path)
        #for class_name in class_label:
        detections = yolov5_output[0].shape[0]
        print('n_detections: ', detections)

        width, height =  image.shape[:2]

        # re-scaling
        x_scale = width / self.resized_imgW
        y_scale = height / self.resized_imgH

        conf_threshold = self.conf_threshold
        score_threshold = self.score_threshold
        nms_threshold = self.nms_threshold

        class_ids = []
        confidences = []
        bboxes = []
      
        for i in range(detections):

            detect = yolov5_output[0][i]
            #print(detect)
            
            getConf = detect[4]
          
            if getConf >= conf_threshold:

            
                class_score = detect[5:]  

                class_idx = np.argmax(class_score)

                if (class_score[class_idx] > score_threshold):

                    # confidence
                    confidences.append(getConf)
                    
                    class_ids.append(class_idx)

                    #get center and w,h coordinates
                    cx, cy, w, h = detect[0], detect[1], detect[2], detect[3]
                   # print("Center X",cx, "Center Y ", cy, " Width", w, "Height: ", h)
                   # print('*********************************************************')
                    #print('\n')
                    

                    # left
                    left = int((cx - w/2) * x_scale)

                    # top
                    top = int((cy - h/2) * y_scale)

                    # width
                    width = int(w * x_scale)

                    #height
                    height = int(h * y_scale) 
                    
                    # box
                    box = np.array([left, top, width, height])

                    # bboxes
                    bboxes.append(box)

        # get max suppresion
        indices = cv2.dnn.NMSBoxes(bboxes, confidences, conf_threshold, nms_threshold)

        for i in indices:
        
            box = bboxes[i]
            left = box[0] 
            top = box[1] 
            width = box[2] 
            height = box[3]

           
            print("Box Left ",left , "Box Top ", top, "Box Width ", width, "Box height: ", height )
            print('*********************************************************')
            print('\n')
        
            print(self.class_labels[class_ids[i]])
            print()
            label = "{}:{:.2f}".format(self.class_labels[class_ids[i]], confidences[i])

            cv2.rectangle(image, (left, top),(left + width, top + height), (0,255,0),3)

            cv2.putText(image, label, (left, top-20), cv2.FONT_HERSHEY_SIMPLEX, 0.7, (0,0,255), 2, cv2.LINE_AA)
           

        cv2.namedWindow('Detect image: ', cv2.WINDOW_NORMAL)
        cv2.resizeWindow('Detect image: ', 640, 640)
        cv2.imshow('Detect image: ', image)
        cv2.waitKey(5000)
        cv2.destroyAllWindows()


def main():
    engine_file_path ='/mnt/c/code/cpp_practice/tensorrt_practice/YoloV7/yolov7.engine'

    # Load the TensorRT engine
    input_shape = (1,3, 640, 640)
    # output_shape = (1, 25500, 7)
    output_shape = (1, 25200, 85)

    image_path = '/mnt/c/code/cpp_practice/tensorrt_practice/YoloV7/Images'

    path_to_class = "/mnt/c/code/cpp_practice/tensorrt_practice/YoloV7/coco.yaml"

    inference = TRTInference(engine_file_path, input_shape, output_shape, path_to_class, 0.38, 0.3, 0.3)

    inference.inference_detection(image_path)

if __name__ == '__main__':
    main()
        