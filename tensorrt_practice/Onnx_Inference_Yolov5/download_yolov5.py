import torch

'''
    在深度学习中，将模型置于评估模式（evaluation mode）对于确保推理（inference）的准确性非常重要。具体原因如下：

    ---

    ### 1. **关闭训练特定的操作**
    在训练模式下（`model.train()`），模型会启用一些训练特定的操作，例如：

    - **Dropout**  
    Dropout 是一种正则化技术，在训练过程中随机屏蔽一部分神经元以防止过拟合。但在推理时，我们需要模型利用所有神经元，因此 Dropout 必须被禁用。

    - **Batch Normalization**  
    Batch Normalization 会根据当前 mini-batch 的均值和方差计算归一化值。在推理时，应该使用整个训练集的均值和方差，而不是当前输入的 mini-batch 的值。

    如果模型处于训练模式，以上这些操作会影响推理的输出结果，导致不准确的预测。

    ---

    ### 2. **确保一致性**
    推理时的行为应该与模型验证阶段（validation）一致，而不是训练阶段。评估模式通过禁用训练相关的机制，确保模型在推理时与验证结果一致。

    ---

    ### 3. **导出 ONNX 的要求**
    在导出 ONNX 模型时，通常需要模型处于评估模式。这是因为 ONNX 模型主要用于推理，ONNX 文件中不会保存训练相关的操作（例如 Dropout 的概率参数）。如果在训练模式下导出，导出的模型可能包含不期望的行为，影响 ONNX 文件的推理性能。

    ---

    ### **示例对比**
    以下示例展示了评估模式和训练模式对 Dropout 的影响：

    ```python
    import torch
    import torch.nn as nn

    # 定义一个简单的模型
    class SimpleModel(nn.Module):
        def __init__(self):
            super(SimpleModel, self).__init__()
            self.dropout = nn.Dropout(p=0.5)

        def forward(self, x):
            return self.dropout(x)

    model = SimpleModel()

    # 输入张量
    x = torch.ones(5)

    # 模型在训练模式下
    model.train()
    print("训练模式输出:", model(x))

    # 模型在评估模式下
    model.eval()
    print("评估模式输出:", model(x))
    ```

    **输出示例**：
    ```plaintext
    训练模式输出: tensor([0., 0., 2., 2., 0.])
    评估模式输出: tensor([1., 1., 1., 1., 1.])
    ```

    可以看到，训练模式下 Dropout 随机屏蔽了一部分神经元，而评估模式下保留了所有神经元。

    ---

    ### **总结**
    在评估模式下：
    - 禁用了 Dropout 和 Batch Normalization 的动态行为。
    - 确保模型推理时的行为与验证阶段一致。
    - 避免 ONNX 导出模型中出现不必要的训练操作。

    因此，在推理和导出模型时，务必要调用 `model.eval()` 将模型切换到评估模式。

'''


'''
    在导出 ONNX 模型时，使用 `torch.device('cpu')` 或 `torch.device('cuda')` 会对生成的模型产生以下影响：

    ### **1. 影响模型的计算设备**
    - **使用 `torch.device('cpu')`**  
    如果你在导出时将模型移至 CPU（`model.to('cpu')`），导出的 ONNX 模型会是一个 **适用于 CPU 推理**的模型。这个模型本身并不依赖 GPU 进行推理，推理时可以在 CPU 上运行。

    - **使用 `torch.device('cuda')`**  
    如果你在导出时将模型移至 GPU（`model.to('cuda')`），导出的 ONNX 模型会包含一些 **与 GPU 相关的计算图**，并且这个模型默认期望在 GPU 上进行推理。如果你导出时使用 GPU，但目标设备是 CPU，推理时可能会出现错误。

    ### **2. 影响输入输出张量的设备**
    在导出过程中，输入张量的设备也会影响模型的导出。模型是基于输入张量的设备生成计算图的，因此输入张量的设备和模型所在的设备需要一致。

    - **在 CPU 上导出**：  
    如果你在 CPU 上导出模型，模型的计算图将适应 CPU 操作（即使模型有 GPU 权重，计算图仍是 CPU 兼容的）。

    - **在 GPU 上导出**：  
    如果你在 GPU 上导出模型，计算图可能会包含 GPU 特定的操作，例如针对 GPU 的内存布局和计算优化（例如，Tensor Cores 计算）。

    ### **3. 模型权重**
    无论你是在 CPU 还是 GPU 上导出模型，权重本身（`model.state_dict()`）是独立于设备的。当你将模型移到不同的设备时，PyTorch 会将权重转换到目标设备（CPU 或 GPU）。例如：

    - **在 GPU 上加载模型，导出时使用 GPU**：权重会存储在 GPU 上，但 PyTorch 会通过 `torch.onnx.export()` 转换为 ONNX 格式时，不会绑定到特定的设备。导出的 ONNX 文件本身没有设备绑定。
    
    - **在 CPU 上加载模型，导出时使用 CPU**：同理，权重会存储在 CPU 上，导出的 ONNX 模型将与 CPU 设备无关。

    ### **4. 对推理设备的影响**
    - **导出时使用 CPU**：  
    如果你导出时使用 `torch.device('cpu')`，则生成的 ONNX 模型在任何设备上（无论是 GPU 还是 CPU）都应该能运行。然而，它可能没有针对 GPU 的优化。例如，TensorRT 优化过程（如 FP16、INT8 加速）只能在 GPU 上进行。

    - **导出时使用 GPU**：  
    如果你导出时使用 `torch.device('cuda')`，生成的 ONNX 模型包含了 GPU 的计算图，推理时通常会更高效，特别是在支持 GPU 加速的环境下（如 TensorRT 或 GPU 版本的 ONNX Runtime）。如果你尝试在 CPU 上运行该模型，则可能无法顺利进行，或者性能显著下降。

    ### **5. 设备和兼容性**
    ONNX 模型本身是设备无关的，但推理时如果使用不同设备（GPU 或 CPU），可能会遇到以下情况：
    - **TensorRT**：如果你使用 TensorRT 进行优化，生成的引擎会基于 GPU 进行硬件优化，不能直接在 CPU 上运行。
    - **ONNX Runtime**：支持在 CPU 和 GPU 上运行 ONNX 模型。对于 GPU 加速的 ONNX Runtime，你可以选择在导出时优化 GPU 操作，但导出的模型本身仍然是设备无关的。

    ### **6. 实际影响**
    总结来说，**导出时使用 `torch.device('cpu')` 或 `torch.device('cuda')` 的影响**：

    - **模型的计算图**：导出时使用 `cuda` 会使计算图与 GPU 兼容，使用 `cpu` 会生成 CPU 兼容的计算图。
    - **推理性能**：在 GPU 上导出并推理，TensorRT 和其他加速库能够更好地优化推理速度。相对地，在 CPU 上导出会导致推理速度较慢。
    - **跨设备兼容性**：在 CPU 上导出模型更具兼容性，适合在 CPU 上推理。而在 GPU 上导出模型则有可能产生与 GPU 相关的操作或内存布局优化，适用于 GPU 推理。

    ---

    ### **总结**
    - **导出时使用 `cpu` 或 `cuda` 主要影响模型计算图的设备兼容性和推理性能**。
    - **如果希望跨设备使用模型，建议使用 `torch.device('cpu')` 导出，确保兼容性**。
    - **如果只在 GPU 上推理，可以使用 `torch.device('cuda')` 导出，以便充分利用 GPU 加速**。
'''

model = torch.hub.load('ultralytics/yolov5', 'custom', 'yolov5s.pt')

device = torch.device('cuda')
model.model.to(device)

image = torch.rand(1, 3, 640, 640).to(device)

model.model.eval()

onnx_file_path = './yolov5s.onnx'

torch.onnx.export(
    model.model,
    image,
    onnx_file_path,
    export_params=True, # 保存权重
    opset_version=12,
    input_names=['images'],
    output_names=['output'],
    dynamic_axes={'images': {0: 'batch'}, 'output': {0: 'batch'}} # 动态batch维度
)

print(f"ONNX 模型已成功导出到 {onnx_file_path}")