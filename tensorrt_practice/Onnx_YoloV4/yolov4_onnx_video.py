'''
    Python packages
    1. os
    2. cv2
    3. time
'''
import os
import cv2
import time

'''
    退出语句的含义

    这行代码：

    ```python
    if cv2.waitKey(5000) & 0xFF == ord('q'):
    ```

    是用来检测键盘输入的，特别是按下“q”键时的响应。让我逐部分解释这行代码的含义：

    ### 1. **`cv2.waitKey(5000)`**
    - `cv2.waitKey()` 是 OpenCV 中用来等待键盘输入的函数。
    - 参数 `5000` 表示等待时间为 5000 毫秒（即 5 秒）。
    - 如果在这段时间内有键盘输入，`cv2.waitKey()` 会返回按下的键的 ASCII 值。如果没有键盘输入，返回值是 `-1`。

    ### 2. **`& 0xFF`**
    - `& 0xFF` 是一个位运算，用来确保我们只关心最低的 8 位（即一个字节）。
    - 在 OpenCV 的不同操作系统中，`cv2.waitKey()` 的返回值可能是更大的数字，包含了更多的额外信息。而 `& 0xFF` 操作的目的是提取返回值的低 8 位（对应单个键的 ASCII 值）。
    
    例如，如果 `cv2.waitKey()` 返回 `1040`（代表某些系统的内部键值），通过 `& 0xFF` 后会变成 `104`，即对应的字符 `'q'` 的 ASCII 值。

    ### 3. **`ord('q')`**
    - `ord('q')` 返回字符 `'q'` 的 ASCII 值，通常是 `113`。

    ### 4. **整个条件语句的意思**
    - `cv2.waitKey(5000)` 等待键盘输入 5 秒，如果在这段时间内没有任何按键输入，`cv2.waitKey(5000)` 会返回 `-1`。
    - 如果在这 5 秒内按下了某个键，它会返回该键的 ASCII 值。
    - `& 0xFF` 是用来保证只取低 8 位的返回值（处理平台之间的差异）。
    - `ord('q')` 返回 `'q'` 的 ASCII 值 `113`，如果按下的键是 `'q'`，那么条件就成立，`if` 语句为 `True`，执行后续的代码（通常是退出程序或关闭窗口）。

    ### 5. **总结**
    这行代码的目的是等待 5 秒钟，查看用户是否按下了“q”键。如果按下了“q”键，则返回 `True`，执行相应的代码。如果没有按下键，等待 5 秒后 `cv2.waitKey(5000)` 会返回 `-1`，条件不成立，不执行任何操作。

    一般情况下，这种代码用来在显示图像窗口时检测用户的退出操作，例如：

    ```python
    if cv2.waitKey(5000) & 0xFF == ord('q'):
        cv2.destroyAllWindows()  # 关闭所有窗口
    ```
'''

'''
    class components
    class name: YoloV4DNN
    class init components: nms_threshold, confidence_threshold, class_labels, video_file, yolov4: [path_to_cfg_yolo, path_to_weights]
'''

class YoloV4DNN:
    # Init of parameters
    def __init__(self, nms_threshold, conf_threshold, class_labels, video_file, path_to_cfg, path_to_weights):
        # non max suppression threshold
        self.nms_threshold = nms_threshold

        # confidence threshold
        self.conf_threshold = conf_threshold

        # video file
        self.video_file = video_file

        # path to configuration yolov4
        self.path_to_cfg = path_to_cfg

        # path to weights yolov4
        self.path_to_weights = path_to_weights

        # read classes coco file with open
        with open(class_labels, 'r') as read_class:
            self.class_labels = [classes.strip() for classes in read_class.readlines()]
        
    def inference_dnn(self, frame, path_to_cfg, path_to_weights):
        # read dnn of yolov4 
        # weights and config
        network = cv2.dnn.readNet(path_to_cfg, path_to_weights)
        # gpu or cpu
        network.setPreferableBackend(cv2.dnn.DNN_BACKEND_CUDA)
        network.setPreferableTarget(cv2.dnn.DNN_TARGET_CUDA_FP16)

        # creates net from file with trained weights and config
        model = cv2.dnn_DetectionModel(network)

        # set model parameters
        model.setInputParams(size=(416, 416), scale=1/255, swapRB=True)

        '''
            classIds	Class indexes in result detection.
            [out]	confidences	A set of corresponding confidences.
            [out]	boxes	A set of bounding boxes.
        '''
        classes, scores, boxes = model.detect(frame, self.conf_threshold, self.nms_threshold)
        return classes, scores, boxes

    '''
        target: Inference run and draw bounding boxes
        param[1]: image
    '''
    def inference_run(self):
        video_cap = cv2.VideoCapture(self.video_file)

        if video_cap.isOpened() is False:
            print('error opening video file')
        
        while video_cap.isOpened():
            grabbed, frame = video_cap.read()
            frame = cv2.resize(frame, (1000, 800))
            if not grabbed:
                exit()
            
            start = time.time()
            # get classes, get boxes, get score
            # inference for every frame
            getClasses, getScores, getBoxes = self.inference_dnn(frame, self.path_to_cfg, self.path_to_weights)
            end = time.time()

            # Frame time
            frame_time = end - start

            # Frame per second 
            FPS = 1.0/ (end - start)

            for (class_id, score, box) in zip(getClasses, getScores, getBoxes):
                cv2.rectangle(frame, box, (0, 255, 0), 2)
                label = "Frame Time : %.2f ms, FPS : %.2f , ID: %s, Score: %.2f," % (frame_time, FPS ,self.class_labels[class_id], score)
                cv2.putText(frame, label, (box[0] - 40, box[1] - 20), cv2.FONT_HERSHEY_SIMPLEX, 0.5, (252, 0, 0), 2)

            cv2.imshow('Image detected: ', frame)

            if cv2.waitKey(2000) & 0xFF == ord('q'):
                break

        video_cap.release()
        cv2.destroyAllWindows()

def main():
    path_to_classes = '/mnt/c/code/cpp_practice/tensorrt_practice/Onnx_YoloV4/coco-classes.txt'
    video_path = '/mnt/c/code/cpp_practice/tensorrt_practice/Onnx_YoloV4/main_demo.mp4'
    path_to_cfg_yolov4 = '/mnt/c/code/cpp_practice/tensorrt_practice/Onnx_YoloV4/yolov4-tiny.cfg'
    path_to_weights_yolov4 = '/mnt/c/code/cpp_practice/tensorrt_practice/Onnx_YoloV4/yolov4-tiny.weights'

    ## call class instance

    yolov4_instance = YoloV4DNN(0.3, 0.38, path_to_classes,video_path , path_to_cfg_yolov4, path_to_weights_yolov4)
    yolov4_instance.inference_run() 

if __name__ == '__main__':
    main()

