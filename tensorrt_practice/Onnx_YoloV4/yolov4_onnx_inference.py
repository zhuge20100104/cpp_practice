'''
    Python packages
    1. os
    2. cv2
    3. time
'''


import os 
import cv2
import time


'''
    class Components
    class name: YoloV4DNN
    class Init components: nms_threshold, confidence_threshold, class_labels, image_path, yolov4: [path_to_cfg_yolo, path_to_weights]
    target: YoloV4 DNN Inference on Images
'''


'''
    两个尺寸的原因

    在你的代码中，YoloV4模型的输入大小设置为`416x416`，尽管你先将输入图像调整为`640x640`，这可能看起来有些混淆。让我解释一下为什么会有这种情况。
    ### 1. **YoloV4 输入的标准尺寸**
    YoloV4模型的标准输入尺寸通常是`416x416`，这是基于YoloV4模型设计时的标准设置。通过输入尺寸为`416x416`的图像，网络能够处理更好的特征提取和准确度。因此，尽管你在代码中将图像调整为`640x640`，这只是在图像加载和处理阶段对图像的预处理（缩放）操作，而在实际的神经网络推理时，仍然使用`416x416`作为输入尺寸。

    ### 2. **Resize到640x640的原因**
    通常，图像在输入到深度学习模型之前都会做一个初步的缩放处理。这里的`640x640`可能是为了保证输入图像能够在各个方面都能够适应模型的要求，减少不同图像尺寸对推理性能的影响。简单来说，缩放到`640x640`可能是为了增强图像的细节，但最终实际推理时还是会使用模型训练时的默认尺寸`416x416`。

    ### 3. **cv2.dnn.DetectionModel的设置**
    在你调用`cv2.dnn_DetectionModel`时，设置了`size=(416, 416)`，这个设置告诉OpenCV的DNN模块，模型的输入尺寸应该是`416x416`，即使你在图像处理阶段将图像调整为`640x640`，最终图像的尺寸会被缩放回`416x416`，确保符合模型的输入要求。

    ### 总结：
    - 图像在进入YoloV4模型之前会根据预设的尺寸进行调整。你首先将图像调整为`640x640`是为了保持图像的比例和细节。
    - 但是YoloV4模型需要`416x416`的输入尺寸，这就是为什么你在调用`model.setInputParams(size=(416, 416))`时，输入的图像会被进一步调整为`416x416`。
    - 所以，`640x640`是一个初步的处理步骤，`416x416`才是最终输入到网络中的尺寸。

    你可以调整`setInputParams`中的`size`参数来尝试使用不同的输入尺寸，但通常`416x416`是最常见的尺寸。如果你想保持`640x640`的输入大小，可能需要在训练模型时使用这个尺寸进行训练。
'''

class YoloV4DNN:
    # Init of parameters
    def __init__(self, nums_threshold, conf_threshold, class_labels, image_path, path_to_cfg, path_to_weights):
        # non max suppression threshold
        self.nms_threshold = nums_threshold

        # confidence threshold
        self.conf_threshold = conf_threshold

        # image path
        self.image_path = image_path

        # path to configuration yolov4
        self.path_to_cfg = path_to_cfg

        # path to weights yolov4
        self.path_to_weights = path_to_weights

        # read class coco files with open
        with open(class_labels, 'r') as read_classes:
            self.class_labels = [classes.strip() for classes in read_classes.readlines() ]
        
        # frame image
        # load images
        self.frames = self.load_images(self.image_path)

        # preprocess images and resize it
        for self.frame in self.frames:
            self.image = cv2.imread(self.frame)

            # get height and width of images
            self.original_h, self.original_w = self.image.shape[:2]
            
            dimension = (640, 640)
            # resized images 
            self.resize_image = cv2.resize(self.image, dimension, interpolation=cv2.INTER_AREA)

            # get new height and width of resized image 
            self.new_h, self.new_w = self.resize_image.shape[:2]

            # call function inference run
            self.inference_run(self.resize_image)
    
    def load_images(self, image_path):
        # list of images
        img_list = []
        for img_original in os.listdir(image_path):
            if img_original.endswith('.jpg') or img_original.endswith('.jpeg') or img_original.endswith('.png'):
                img_full_path = os.path.join(image_path, img_original)
                img_list.append(img_full_path)
        return img_list
    
    def inference_dnn(self, path_to_cfg, path_to_weights):
        # read dnn of yolov4 
        # weights and config
        network = cv2.dnn.readNet(path_to_cfg, path_to_weights)
        # gpu or cpu
        network.setPreferableBackend(cv2.dnn.DNN_BACKEND_CUDA)
        network.setPreferableTarget(cv2.dnn.DNN_TARGET_CUDA_FP16)
        
        # create net from the file with trained weights and config
        model = cv2.dnn_DetectionModel(network)

        # set model parameters
        model.setInputParams(size=(416, 416), scale=1/255, swapRB=True)

        # YoloV4 ONNX model的返回值
        '''
            classIds	Class indexes in result detection.
            [out]	confidences	A set of corresponding confidences.
            [out]	boxes	A set of bounding boxes.
        '''

        classes, scores, boxes = model.detect(self.image, self.conf_threshold, self.nms_threshold)

        return classes, scores, boxes
    
    def inference_run(self, image):
        # Start
        start = time.time()

        # get classes, get boxes, get score
        # inference for every frame
        getClasses, getScores, getBoxes = self.inference_dnn(self.path_to_cfg, self.path_to_weights)

        end = time.time()

        # frame time
        frame_time = (end - start)

        # frame per second
        FPS = 1.0/frame_time

        # calculate new scale of image which is image formed between original and resized
        # new image ratio height
        radio_h = self.new_h / self.original_h

        # new image ratio width
        radio_w = self.new_w / self.original_w

        for (class_id, score, box) in zip(getClasses, getScores, getBoxes):

            print(f'Box coordinates: ', box)

            # normalize bounding box to detection
            box[0] = int(box[0] * radio_w)
            box[1] = int(box[1] * radio_h)
            box[2] = int(box[2] * radio_w)
            box[3] = int(box[3] * radio_h)

            cv2.rectangle(image, box, (0, 255, 0), 2)
            label = 'Frame time: %.2f ms, FPS: %.2f, ID: %s, Score: %.2f,' % (frame_time, FPS, self.class_labels[class_id], score)
            cv2.putText(image, label, (box[0] - 30, box[1] - 10), cv2.FONT_HERSHEY_SIMPLEX, 0.5, (252, 0, 0), 2)
        cv2.imshow('Image Detected: ', image) 
        cv2.waitKey(8000)
        cv2.destroyAllWindows()

def main():
    path_to_classes = '/mnt/c/code/cpp_practice/tensorrt_practice/Onnx_YoloV4/coco-classes.txt'
    image_path = '/mnt/c/code/cpp_practice/tensorrt_practice/Onnx_YoloV4/Images'
    path_to_cfg_yolov4 = '/mnt/c/code/cpp_practice/tensorrt_practice/Onnx_YoloV4/yolov4-tiny.cfg'
    path_to_weights_yolov4 = '/mnt/c/code/cpp_practice/tensorrt_practice/Onnx_YoloV4/yolov4-tiny.weights'

    # call class instance
    YoloV4DNN(0.3, 0.38, path_to_classes, image_path, path_to_cfg_yolov4, path_to_weights_yolov4)        

if __name__ == '__main__':
    main()