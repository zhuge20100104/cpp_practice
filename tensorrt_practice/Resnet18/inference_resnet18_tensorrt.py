'''
    Preprocessing of libraries stage
    import tensorrt
    import pycuda
    import cv2
    import numpy as np and etc
'''
import tensorrt as trt
import pycuda.autoinit

import pycuda.driver as cuda
import cv2
import numpy as np

import os
from PIL import Image
import matplotlib.pyplot as plt


'''
    class name: TRTInference
    INIT: self.logger
    params[1]: engine_path, context
    params[2]: input shape and output shape, class_labels
'''


'''
    img_np.transpose((2, 0, 1))的作用 --


    `img_np.transpose((2, 0, 1))` 的作用是对图像数组的维度进行变换，以符合模型输入的形状要求。以下是具体原因和背景解释：

---

### 图像数据的默认排列
通常，`Pillow` 或其他常见的图像处理库（如 OpenCV）会将图像数据表示为一个 NumPy 数组，其形状为：

- **`(H, W, C)`**：高度（Height）、宽度（Width）、通道数（Channels）

例如，彩色图像（RGB）的形状可能是 `(224, 224, 3)`。

---

### 深度学习模型的输入排列
大多数深度学习框架（如 PyTorch、TensorFlow 等）要求输入的图像数据形状为：

- **`(N, C, H, W)`**：
  - `N` 是批次大小（Batch size）
  - `C` 是通道数（Channels，如 RGB 通道数量为 3）
  - `H` 是图像的高度（Height）
  - `W` 是图像的宽度（Width）

对于单张图像，`N` 通常为 `1`，因此，单张图像的输入形状为 `(1, C, H, W)`。

---

### 为什么使用 `transpose`
`transpose((2, 0, 1))` 是将图像的维度从 `(H, W, C)` 转换为 `(C, H, W)`，即：

1. **输入**：`(224, 224, 3)` （高度、宽度、通道数）
2. **输出**：`(3, 224, 224)` （通道数、高度、宽度）

这是为了将图像的形状调整为深度学习模型所需的格式。

---

### 数据处理步骤解释
以下是代码的处理步骤以及原因：

1. **`np.array(img)`**  
   将图像转换为 NumPy 数组，形状为 `(H, W, C)`。

2. **`.astype(np.float32) / 255.0`**  
   将像素值从 `[0, 255]` 缩放到 `[0, 1]`，这是深度学习模型常见的归一化步骤。

3. **`transpose((2, 0, 1))`**  
   将通道维度移动到第一个位置，形状变为 `(C, H, W)`。

4. **`np.expand_dims(img_np, axis=0)`**  
   增加批次维度，形状变为 `(1, C, H, W)`，以符合模型的输入要求。

---

### 示例对比
假设原始图像形状为 `(224, 224, 3)`：

- **未使用 `transpose`**：  
  如果直接传入模型，可能会导致输入维度不匹配的错误（例如，模型需要 `(C, H, W)`）。

- **使用 `transpose`**：  
  转换后的形状为 `(3, 224, 224)`，符合模型的通道优先格式要求。

---

### 总结
`img_np.transpose((2, 0, 1))` 是为了将图像从常见的通道最后（`HWC`）格式转换为深度学习模型常用的通道优先（`CHW`）格式。这个转换在处理图像数据时非常常见，因为大多数深度学习框架（如 PyTorch）使用的是通道优先格式。
'''

class TRTInference:
    def __init__(self, engine_file_path, input_shape, output_shape, class_labels_file):
        self.logger = trt.Logger(trt.Logger.WARNING)
        
        self.engine_file_path = engine_file_path
        # load engine
        self.engine = self.load_engine(self.engine_file_path)
        
        # init context
        self.context = self.engine.create_execution_context()

        self.input_shape = input_shape
        self.output_shape = output_shape
        self.class_labels_file = class_labels_file
        
        with open(self.class_labels_file, 'r') as class_read:
            self.class_labels = [line.strip() for line in class_read.readlines()]
    
    def load_engine(self, engine_file_path):
        with open(engine_file_path, 'rb') as f:
            runtime = trt.Runtime(self.logger)
            engine_deserialized = runtime.deserialize_cuda_engine(f.read())
        return engine_deserialized

    '''
        params[1]: image path
        results(return): img_list, img_path
        img resolution: 1, 3, 224, 224
    '''
    def preprocess_img(self, image_path):
        img_list = []
        img_path = []

        for img_original in os.listdir(image_path):
            if img_original.endswith('.jpg') or img_original.endswith('.png') or img_original.endswith('.jpeg'):
                img_full_path = os.path.join(image_path, img_original)

                # open image
                image = Image.open(img_full_path)

                # resize image
                # [224, 224]
                img = image.resize((self.input_shape[2], self.input_shape[3]), Image.NEAREST)
                img_np = np.array(img).astype(np.float32)/ 255.0
                img_np = img_np.transpose((2, 0, 1))
                img_np = np.expand_dims(img_np, axis=0)
                img_list.append(img_np)
                img_path.append(img_full_path)

        return img_list, img_path
    
    # Inference detection
    '''
        param[0]: self
        param[1]: image_path
        target: Inference Detection on GPU Local
    ''' 
    def inference_detection(self, image_path):
        input_list, full_image_paths = self.preprocess_img(image_path)
        results = []
    
        for inputs, full_image_path in zip(input_list, full_image_paths):
            inputs = np.ascontiguousarray(inputs)
            outputs = np.empty(self.output_shape, dtype=np.float32)

            d_inputs = cuda.mem_alloc(1 * inputs.nbytes)
            d_outputs = cuda.mem_alloc(1 * outputs.nbytes)
            bindings = [d_inputs, d_outputs]
            
            # transfer input to gpu
            cuda.memcpy_htod(d_inputs, inputs)

            # sychronize
            self.context.execute_v2(bindings)

            # copy output backto host (cpu)
            cuda.memcpy_dtoh(outputs, d_outputs)

            result = self.postprocess_img(outputs)

            d_inputs.free()
            d_outputs.free()

            # results 
            results.append(result)
            
            # display results
            self.display_recognized_images(full_image_path, result)
        
        del self.context
        del self.engine
        return results 
    

    # postprocessing labels with corresponding images
    def postprocess_img(self, outputs):
        # classes
        classes_indices = []

        for output in outputs:
            class_idx = output.argmax()
            print('Class detected: ', self.class_labels[class_idx])
            classes_indices.append(self.class_labels[class_idx])
        return classes_indices

    '''
        param[0]: image_path
        param[1]: class_label
        target: Displaying and saving detected images
    '''
    def display_recognized_images(self, image_path, class_label):
        image = Image.open(image_path)

        for class_name in class_label:
            # create one directory for detected images
            path_to_detected_imgs = '/mnt/c/code/cpp_practice/tensorrt_practice/Resnet18/images_detected'

            # check path existence
            if not os.path.exists(path_to_detected_imgs):
                os.makedirs(path_to_detected_imgs)
            
            plt.imshow(image)
            plt.title('Recognized image: {}'.format(class_name))
            plt.axis('off')

            save_img = os.path.join(path_to_detected_imgs, '{}.jpg'.format(class_name))
            plt.savefig(save_img)

            plt.show()
            plt.close()
                


if __name__ == '__main__':
    engine_file_path = '/mnt/c/code/cpp_practice/tensorrt_practice/Resnet18/resnet.engine'
    # 是否需要改成224, 224
    input_shape = (1, 3, 224, 224)
    output_shape = (1, 1000)
    class_labels = '/mnt/c/code/cpp_practice/tensorrt_practice/Resnet18/imagenet-classes.txt'
    path_to_origin_imgs = '/mnt/c/code/cpp_practice/tensorrt_practice/Resnet18/Images'

    inference = TRTInference(engine_file_path, input_shape, output_shape, class_labels)
    class_name = inference.inference_detection(path_to_origin_imgs)
    print(class_name)