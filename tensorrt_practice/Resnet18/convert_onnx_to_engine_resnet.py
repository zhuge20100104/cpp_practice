'''
    Target: TensorRT conversion
    model: Resnet 18
    Image Resolution: [1, 3, 224, 224]
    Tensorrt:  - TODO
    Python3: OOP Methlology
'''


import tensorrt as trt

'''

explicit_bath和set_shape联合使用的因由 --- By ChatGPT

在TensorRT中，`explicit_batch` 和 `DynamicBatch` 都与批量大小的动态调整相关，但它们的含义和用途有所不同，具体情况可以从以下几点详细分析：

### 1. `explicit_batch` (显式批次)
这是TensorRT中与批次维度处理相关的一个配置标志。TensorRT通过使用`explicit_batch`来明确指出网络是动态批量大小的，也就是说，输入的批次维度（batch size）可以在推理时变化。

- `explicit_batch = 1 << int(trt.NetworkDefinitionCreationFlag.EXPLICIT_BATCH)` 这一行代码的作用是通过设置`EXPLICIT_BATCH`标志来告诉TensorRT你在使用动态批量大小，并且可以在推理时指定不同的批量大小。

### 2. `profile.set_shape()` 和 `DynamicBatch`
`create_optimization_profile()` 和 `set_shape()` 是TensorRT优化器中用于设置输入张量形状范围的配置函数。它们主要用于指定输入形状的最小、最优和最大维度，在推理过程中支持动态输入形状（例如不同的批次大小）。

- `profile.set_shape('input.1', min=(1, 3, 224, 224), opt=(10, 3, 224, 224), max=(20, 3, 224, 224))` 的意思是指定了输入张量`input.1`的动态维度范围。`min`是批次大小的最小值，`opt`是最优值，`max`是最大值。这里的意思是，你的输入张量支持批次大小在1到20之间变化，最优批次大小是10。

**DynamicBatch**的含义是指TensorRT在推理时允许在指定范围内调整批次大小。

### 3. 冲突分析
`explicit_batch`和`DynamicBatch`的配置不会冲突，因为它们分别控制了网络定义和输入形状的动态批量大小。

- `explicit_batch`（通过`EXPLICIT_BATCH`标志）表示网络能够接受动态的批量大小。
- `DynamicBatch`（通过`create_optimization_profile()`和`set_shape()`配置）允许在推理时根据实际情况调整输入张量的批量大小。

这些配置共同作用，允许TensorRT根据实际推理时的需求动态调整批次大小，而不会发生冲突。

### 举例说明

假设你有一个图像分类模型，输入是一个形状为 `(batch_size, 3, 224, 224)` 的张量，其中`batch_size`是动态变化的。你可以设置：

```python
explicit_batch = 1 << int(trt.NetworkDefinitionCreationFlag.EXPLICIT_BATCH)
```

这告诉TensorRT该网络支持动态批量大小。然后，通过以下配置为优化提供动态形状支持：

```python
profile = builder.create_optimization_profile()
profile.set_shape('input.1', min=(1, 3, 224, 224), opt=(10, 3, 224, 224), max=(20, 3, 224, 224))
config.add_optimization_profile(profile)
```

- 最小批次大小为1，最优批次大小为10，最大批次大小为20。
- 在推理时，你可以根据需求使用1到20之间的任意批次大小，TensorRT会根据配置的最优批次大小来优化性能。

因此，`explicit_batch`为动态批量大小的启用提供支持，而`set_shape`则定义了不同批次大小下输入的形状范围。两者配合使用，使得模型可以灵活地调整批量大小以获得最佳推理性能。

'''

class TensorRTConversion:
    '''
    path_to_onnx: onnx path
    path_to_engine: engine path
    maxworkspace: 1 << 30 - 1<<10 = 1024   /1024/1024/1024 = 1GB  < 1GB 
    precision: 16 float and half precision
    Inference mode: Dynamic batch [1, 10, 20]
    '''
    def __init__(self, path_to_onnx, path_to_engine, max_workspace_size=1 << 30, half_precision=False):
        self.TRT_LOGGER = trt.Logger(trt.Logger.WARNING)
        self.path_to_onnx = path_to_onnx
        self.path_to_engine = path_to_engine
        self.max_workspace_size = max_workspace_size
        self.half_precision = half_precision

    '''
        {
            INIT BUILD
            INIT CONFIG
            INIT EXPLICIT BATCH
            INIT NETWORK
        }
        Tensorrt >= 8.0.0
    '''
    def convert(self):
        builder = trt.Builder(self.TRT_LOGGER)
        config = builder.create_builder_config()
        config.set_memory_pool_limit(trt.MemoryPoolType.WORKSPACE, self.max_workspace_size)
        # config.max_workspace_size = self.max_workspace_size
        explicit_batch = 1 << int(trt.NetworkDefinitionCreationFlag.EXPLICIT_BATCH)
        network = builder.create_network(explicit_batch)
        parser = trt.OnnxParser(network, self.TRT_LOGGER)

        with open(self.path_to_onnx, 'rb') as model_onnx:
            if not parser.parse(model_onnx.read()):
                print('ERROR: Failed to parse Onnx Model')
                for error in parser.errors:
                    print(error)
                return 
        
        # Set profile for explicit batch
        profile = builder.create_optimization_profile()
        profile.set_shape('input.1', min=(1, 3, 224, 224), opt=(1, 3, 224, 224), max=(1, 3, 224, 224))
        config.add_optimization_profile(profile)

        print('Successfully TensorRT Engine Configured to Max Batch')
        print('\n')

        if builder.platform_has_fast_fp16:
            config.set_flag(trt.BuilderFlag.FP16)
        
        engine = builder.build_serialized_network(network, config)
        with open(self.path_to_engine, 'wb') as f_engine:
            f_engine.write(engine)
        
        print('Successfully convert ONNX to TensorRT Dynamic Engine')
        print('Serialized engine saved in engine path: {}'.format(self.path_to_engine))
    

if __name__ == '__main__':
    convert = TensorRTConversion('/mnt/c/code/cpp_practice/tensorrt_practice/Resnet18/resnet18.onnx', 
                                 '/mnt/c/code/cpp_practice/tensorrt_practice/Resnet18/resnet.engine')
    convert.convert()