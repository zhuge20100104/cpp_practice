import torch

'''
    在深度学习中，将模型置于评估模式（evaluation mode）对于确保推理（inference）的准确性非常重要。具体原因如下：

    ---

    ### 1. **关闭训练特定的操作**
    在训练模式下（`model.train()`），模型会启用一些训练特定的操作，例如：

    - **Dropout**  
    Dropout 是一种正则化技术，在训练过程中随机屏蔽一部分神经元以防止过拟合。但在推理时，我们需要模型利用所有神经元，因此 Dropout 必须被禁用。

    - **Batch Normalization**  
    Batch Normalization 会根据当前 mini-batch 的均值和方差计算归一化值。在推理时，应该使用整个训练集的均值和方差，而不是当前输入的 mini-batch 的值。

    如果模型处于训练模式，以上这些操作会影响推理的输出结果，导致不准确的预测。

    ---

    ### 2. **确保一致性**
    推理时的行为应该与模型验证阶段（validation）一致，而不是训练阶段。评估模式通过禁用训练相关的机制，确保模型在推理时与验证结果一致。

    ---

    ### 3. **导出 ONNX 的要求**
    在导出 ONNX 模型时，通常需要模型处于评估模式。这是因为 ONNX 模型主要用于推理，ONNX 文件中不会保存训练相关的操作（例如 Dropout 的概率参数）。如果在训练模式下导出，导出的模型可能包含不期望的行为，影响 ONNX 文件的推理性能。

    ---

    ### **示例对比**
    以下示例展示了评估模式和训练模式对 Dropout 的影响：

    ```python
    import torch
    import torch.nn as nn

    # 定义一个简单的模型
    class SimpleModel(nn.Module):
        def __init__(self):
            super(SimpleModel, self).__init__()
            self.dropout = nn.Dropout(p=0.5)

        def forward(self, x):
            return self.dropout(x)

    model = SimpleModel()

    # 输入张量
    x = torch.ones(5)

    # 模型在训练模式下
    model.train()
    print("训练模式输出:", model(x))

    # 模型在评估模式下
    model.eval()
    print("评估模式输出:", model(x))
    ```

    **输出示例**：
    ```plaintext
    训练模式输出: tensor([0., 0., 2., 2., 0.])
    评估模式输出: tensor([1., 1., 1., 1., 1.])
    ```

    可以看到，训练模式下 Dropout 随机屏蔽了一部分神经元，而评估模式下保留了所有神经元。

    ---

    ### **总结**
    在评估模式下：
    - 禁用了 Dropout 和 Batch Normalization 的动态行为。
    - 确保模型推理时的行为与验证阶段一致。
    - 避免 ONNX 导出模型中出现不必要的训练操作。

    因此，在推理和导出模型时，务必要调用 `model.eval()` 将模型切换到评估模式。

'''


model = torch.hub.load('ultralytics/yolov5', 'custom', 'yolov5s.pt')

device = torch.device('cpu')
model.model.to(device)

image = torch.rand(1, 3, 640, 640)

model.model.eval()

onnx_file_path = './yolov5s.onnx'

torch.onnx.export(
    model.model,
    image,
    onnx_file_path,
    export_params=True, # 保存权重
    opset_version=12,
    input_names=['images'],
    output_names=['output'],
    dynamic_axes={'images': {0: 'batch'}, 'output': {0: 'batch'}} # 动态batch维度
)

print(f"ONNX 模型已成功导出到 {onnx_file_path}")